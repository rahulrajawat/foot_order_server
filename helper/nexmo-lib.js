var http = require('http');

var https = require('https');
// Secured Based Credentails Here Put ....
/*
 *  @Api Key must be required,this api key provided nexmo sdk which have secured and private based 
 *  @Api Secret Key also most secret key which also provide uniquly get in nexmo sdk
 *  @Verify Mobile No For Access This api or request 
 *  @Resonse Must be {request_id,status =0};
 *  @Status 0 Means Successfully Response...
 */
var api_key = process.env.api_key || "22380a66";
//Api Key here share For Sdk...
var api_secret = process.env.api_secret || '065f7d8a8655b683';
// Data Convert as string for provide A String Or Nexmo Sdk ....
var host = process.env.host || 'api.nexmo.com';
// Host Here Provided For the Hosting Nexmo Sdk...
var port = process.env.port || 443;
// Dynamic Port Here Provide
var brand = 'Qr-App';
// Brand Name Here Included...

module.exports.sendOtpByUsers = function(phone_no, callback) {
    /*
     *  @Purpose   :   Otp Send  For Verify User
     *  @Required :    Mobile No with country code i.e 91 
     *  @Notes  :  Country Code Must be Used Without + or any arithmatic syntex.   
     */
    var data = JSON.stringify({
        api_key: api_key, // Api Key 
        api_secret: api_secret, // Secret Key
        number: phone_no, // Dynmaci mobile no.
        brand: 'Qr-App' // My Brand Name 
    });
    // Response Data which provided Sdk 

    var options = {
        host: host, // Host Name Your Nexmo sdk...
        path: '/verify/json', // Nexmo Method ...
        port: port, // Your Nexmo Default Port...
        method: 'POST', // Method Type You Sdk ..
        headers: { // Header for  the sdk...
            'Content-Type': 'application/json',
            'Content-Length': Buffer.byteLength(data)
        }
    };
    // Passed To Option Here Your Option Which Your Provide.. 
    var req = https.request(options);
    // Write data here...
    req.write(data);
    // Response End Here...
    req.end();
    // Default Set as String For the make globle String...
    var responseData = '';
    //Req On Method Here set...
    req.on('response', function(res) {
        // Data Here get ///
        ////console.log(res)
        res.on('data', function(chunk) {
            responseData += chunk;
        });

        res.on('end', function() {
            // //console.log(JSON.parse(responseData));
            // Callback Here...
            // Data Here Call back Return ...
            callback(responseData);

        });
    });

}
// End The Function SendOtp 

module.exports.verifyUser = function(nexmo_id, otp, callback) {
    //console.log("verify user is called.here");
    var unirest = require("unirest");

    var req = unirest("POST", "https://api.nexmo.com/verify/check/json");

    req.headers({
        "postman-token": "5add9a1f-2cd3-65f3-a42d-70c70151c6ec",
        "cache-control": "no-cache",
        "content-type": "application/json"
    });

    req.type("json");
    req.send({
        "api_key": api_key,
        "api_secret": api_secret,
        "request_id": nexmo_id,
        "code": otp
    });

    req.end(function(res) {
        //if (res.error) throw new Error(res.error);

        //console.log(res.body);
        callback(res.body);
    });
    //console.log("verify user");

}