
//////////////////////////////////////////////////////////////////////////////////////////////////
module.exports.arrayArange =function(params,arrayParams,da){
	////console.log(arrayParams.user_id);
	 var responseArray ={};
	 for (var i = 0; i <params.length; i++) {
      if(params[i] in arrayParams ){
         responseArray[params[i]]  =arrayParams[params[i]]; 
      }
   
     }
    return responseArray;
}

//////////////////////////////////////////////////////////////////////////////////////////////////

module.exports.ValidArrayArange =function(params,arrayParams,da){
	////console.log(arrayParams.user_id);
	 var responseArray ={};
	 for (var i = 0; i <params.length; i++) {
      if(params[i] in arrayParams && arrayParams[params[i]]!=""){
         responseArray[params[i]] = arrayParams[params[i]]; 
      }
   
     }
    return responseArray;
}

//////////////////////////////////////////////////////////////////////////////////////////////////


module.exports.GetUserData =function(data,base_url){
     var GetUserData =[];
     var GetUserData1 ={};
     let i =0;
     ////console.log("hello i am here");
   
        data.forEach(function(row){
          
            var profile_pic =""; 
            // Profile Pic Assign Empty
            var is_verify =false;
            // Is Verify Assign By  Default False
            var gender ="";
            // Gender Assign Empty....
            var country_code =0;
            // Country Code Assign Empty....
            var access_token ="";

            //here Device Token Assign Empty
            var device_token ="";
            // Deefind By Default Empty Here for If Profile Pic Not Found This One Condtion
            
          
            if(row.profile_pic!==undefined && row.profile_pic!==""){
              profile_pic= base_url+"/profile_pic/"+row.profile_pic;
            }
            GetUserData1['username'] =(row.username!="")?(row.username):("");
            GetUserData1['user_id'] =row._id;
            GetUserData1['profile_pic'] =profile_pic;
            GetUserData1['gender'] = (row.gender!="" && row.gender ==undefined)?(row.gender):("");
            GetUserData1['phone_no'] = (row.phone_no!="")?(row.phone_no):("");
            GetUserData1['country_code'] = (row.country_code!="")?(row.country_code):("");
            GetUserData1['is_verify'] = (row.is_verify && row.is_verify!=="" && row.is_verify !==undefined || is_verify in row)?(row.is_verify):(0);
            GetUserData1['access_token'] = (row.access_token!="" && row.access_token !==undefined)?(row.access_token):("");
            GetUserData1['device_token'] = (row.device_token!="" && row.device_token !==undefined)?(row.device_token):("");
            GetUserData1['user_lat'] = (row.user_lat!="" && row.user_lat !==undefined)?(row.user_lat):("");
            GetUserData1['user_long'] = (row.user_long!="" && row.user_long !==undefined)?(row.user_long):("");
            GetUserData1['country'] = (row.country!="" && row.country !==undefined)?(row.country):("");
            GetUserData1['shouldSpeakEnglish'] = (row.shouldSpeakEnglish!="" && row.shouldSpeakEnglish !==undefined)?(row.shouldSpeakEnglish):(0);
            GetUserData1['auth_type'] = (row.auth_type!="" && row.auth_type !==undefined)?(row.auth_type):(0);
            //GetUserData1['auth_type'] = (row.auth_type!="" && row.auth_type !==undefined)?(row.auth_type):(false);
            GetUserData1['language'] = (row.language!="" && row.language !==undefined)?(row.language):("");
            GetUserData1['date_of_birth'] = (row.date_of_birth!="" && row.date_of_birth !==undefined)?(row.date_of_birth):("");
            GetUserData1['login_status'] = (row.login_status!="" && row.login_status !==undefined)?(row.login_status):(1);
            GetUserData1['currency'] = (row.currency!="" && row.currency !==undefined)?(row.currency):("$");
             GetUserData1['push_notification_enable'] = (row.push_notification_enable!="" && row.push_notification_enable !==undefined)?(row.push_notification_enable):(true);
             GetUserData1['range'] = (row.location_range!="" && row.location_range !==undefined)?(row.location_range):(50);
             GetUserData1['city'] = (row.city!="" && row.city !==undefined)?(row.city):("");
             GetUserData1['address'] = (row.address!="" && row.address !==undefined)?(row.address):("");
             GetUserData1['shouldLocationEnable'] = (row.shouldLocationEnable!="" && row.shouldLocationEnable !==undefined)?(row.shouldLocationEnable):(1);
             GetUserData1['avg_rating'] = (row.shouldLocationEnable!="" && row.shouldLocationEnable !==undefined)?(row.shouldLocationEnable):(1);
             GetUserData1['defaultCard'] = (row.defaultCard!="" && row.defaultCard !==undefined)?(row.defaultCard):null;
            GetUserData[i] =GetUserData1;
            i++;
            GetUserData1 ={};
        });
    return GetUserData;
  
}

////////////////////////////////////////////////////////////////////////////////////////
//module.exports.updateArray;


module.exports.push_notification = function(device_token, subject, message, callback) {
    var apn = require('apn');

    var cert_and_key = require('fs').readFileSync('/var/www/QR-IT/newQR1909/helper/dev_APNs_Certificates.pem')
    notifier = require('node_apns').services.Notifier({
        cert: cert_and_key,
        key: cert_and_key
    }, true /* development = true, production = false */ )
    // var FCM = require('fcm-node');
    // var serverKey = 'AAAA93rhv6s:APA91bE6V7pdnOctmotj15JgH3hwlVWWnG9J5s8S9Rowhat-fTN7Z3E3QFwimBWmF-eyXdvRvM26hdZ0fVGqiyAFlvihgnWigQ0OxOygPD5f1r7WNq5RT-kVX7AbbZedI-roGJWNbQnh'; //put your server key here 
    // var fcm = new FCM(serverKey);
    // //console.log('device_token length ',device_token.length);

    
    /*var androidMessage = { //this may vary according to the message type (single recipient, multicast, topic, et cetera) 
        to: device_token,
        notification: {
            title: subject,
            body: message
        },

    };*/
    /* 
    Now you may send notifications!
    */

    var Notification = require('node_apns').Notification;

    notifier.notify(Notification(device_token, {
            aps: {
                alert: subject,
                sound: "default",
                message: message
            }
        }),
        function(err) {
            /*fcm.send(androidMessage, function(err, response) {
                if (err) {
                    //console.log("Something has gone wrong!",err, response);
                    callback(true);
                } else {
                    //console.log("Successfully sent with response: ", response);
                    callback(true);
                }
            });*/
            // if (!err) //console.log("ios Sent", this); 
            // else //console.log('ios Error', err, 'on', this);
            callback(true);
        }
    );

}


//====================================================================================