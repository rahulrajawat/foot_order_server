const nodemailer = require("nodemailer")
const include = require("./include")
// var EmailTemplate = require('email-templates').EmailTemplate;
var transporter = nodemailer.createTransport({
    service: "gmail",
    auth: {
        user: include.titansemail,
        pass: include.titanspass
    }
});

const Email = require('email-templates');

const email = new Email({
    message: {
        from: include.titansemail
    },
    // uncomment below to send emails in development/test env:
    send: true,
    transport: transporter,
    views: {
        options: {
            extension: 'ejs' // <---- HERE
        }
    }
});



module.exports = {
    getTemplate: function (name) {
        return `../html_templates/${name}`
    },
    sendEmail: function (emails, subject, template, name = "", locals) {

        console.log("inside sendemail", this.getTemplate(template));

        return new Promise((resolve, reject) => {
            email
                .send({
                    template: this.getTemplate(template),
                    message: {
                        subject: subject,
                        to: emails
                    },
                    locals: locals ? locals : {
                        name: name
                    },
                    // subjectPrefix: subject
                })
                .then((success) => {
                    resolve(success)
                })
                .catch(reject);
        });
    }

}