// stripe key katrina's account TEST keys
module.exports.stripekey = require("stripe")(
  "sk_test_6gngMkXDHTUg6vZvHs5ldUzf"
); //private key
module.exports.stripekeypublic = require("stripe")(
  "pk_test_FC3EDLIdeQdCv6sqsOE5UAod"
); //public key

// stripe key katrina's account LIVE keys
module.exports.stripekeylive = require('stripe')('sk_live_ypEbBwSeFijsxn3ZYzYwGeQw'); //private key
module.exports.stripekeypubliclive = require('stripe')('pk_live_TkzcIxgU4Zklgv6LpR9Er0Jz'); //public key

module.exports.titansemail = "queries@titanssecret.com.au"
module.exports.titanspass = "successful10"

module.exports.auth = require("../helper/auth-lib");
//Common Helper....
module.exports.common_helper = require("../helper/common");
module.exports.required = require("../lib/required");

// Message Purpsoe Thing  Data must be Required....
module.exports.message = require("../lib/message");

module.exports.utils = require("./util");

module.exports.constant = require("../lib/constant");
//User  Defined Here.......
module.exports.UserModel = require("../model/User-Model/User.js");

//User Schema Defined Here.......
module.exports.UserSchema = require("../model/User-Model/UsersSchema.js");

//User Schema Defined Here.......
module.exports.EndUserSchema = require("../model/User-Model/EndUserSchema.js");

//Notification Schema Defined Here.......
module.exports.MasterCategorySchema = require("../model/MasterCategory-Model/mastercategory_schema.js");

//Stripe Card Info Add

module.exports.KitchenModel = require("../model/Kitchen-Model/Kitchen_menu.js");

//Stripe Schema Defined Here.......
module.exports.KitchenSchema = require("../model/Kitchen-Model/Kitchen_Schema.js");

//Transaction info
module.exports.TrnsxSchema = require("../model/Transactions/trnsx_schema.js");

//Required Defined Here.......
module.exports.HTTP_RESPONSE = require("../lib/http_response");

// ValidTion Must Be ReQuired Data Must Be Required....................
module.exports.UserNameIsInvalid = "UserNameIsInvlide";

// Password Is Invalid Condtion......
module.exports.PasswordIsInvalid = "PasswordIsInvlide";

// User Name is Invalid....
module.exports.LoginSuccess = "LoginSuccess";

module.exports.kitchenFeature_schema = require("../model/KitchenFeature-Model/kitchenFeature_schema");

module.exports.stripe_model = require("../model/Stripe/stripe_model.js");
module.exports.countrySchema = require("../model/Stripe/country_schema.js");

//Wallet Schema and model
module.exports.WalletModel = require("../model/Wallet/wallet_model.js");
module.exports.wallet_schema = require("../model/Wallet/wallet_schema.js");

//Order Schema and model
module.exports.OrderModel = require("../model/Order/Order_model.js");
module.exports.Order_schema = require("../model/Order/OrderSchema.js");
module.exports.OrderItem_schema = require("../model/Order/OrderItem_schema.js");
module.exports.Payment_schema = require("../model/Order/Payment_schema.js");