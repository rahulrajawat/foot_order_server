module.exports.success =function(message){
  var array = {
  	//'method_name' : method_name,
    'success'     : true,
    'errors'      : false,
    'status'      : 200,
    'message'     : message
  };
  //callback(array,null);
  return array;
}
//============================================================================================
module.exports.successWithResponse =function(message,responseData){
  var array = {
  	//'method_name' : method_name,
    'success'     : true,
    'errors'      : false,
    'status'      : 200,
    'message'     : message,
    'response'    : responseData
  };
  return array;
}

module.exports.errorWithResponse =function(message,responseData){
  var array = {
    //'method_name' : method_name,
    'success'     : false,
    'errors'      : true,
    'status'      : 500,
    'message'     : message,
    'response'    : responseData
  };
  return array;
}

//============================================================================================
module.exports.UserAlreadyExists =function(message,responseData){
  var array = {
    //'method_name' : method_name,
    'success'     : false,
    'errors'      : true,
    'status'      : 200,
    'message'     : message,
    'response'    : responseData
  };
  return array;
}
//===========================================================================================
module.exports.errorsWithMessage =function(message,response){
  var array = {
  	/*'method_name' : method_name,*/
    'success'     : false,
    'errors'      : true,
    'status'      : 500,
    'message'     : message,
    'response'    : response  
  };
  return array;
}
//============================================================================================
module.exports.required =function(method_name,parameters){
  var array = {
  
    'success'     : false,
    'errors'      : true,
    'status'      : 404,
    'message'     : parameters+ "is required."
  };
 return array;
}
//============================================================================================
module.exports.requiredWithMessage =function(message){
  var array = {
  
    'success'     : false,
    'errors'      : true,
    'status'      : 404,
    'message'     : message
  };
 return array;
}
//============================================================================================
module.exports.alreadyExistsMessage =function(message){
  var array = {
    /*'method_name' : method_name,*/
    'success'     : false,
    'errors'      : true,
    'status'      : 402,
    'message'     : message,
    'response'    : []
  };
  return array;
}


//============================================================================================