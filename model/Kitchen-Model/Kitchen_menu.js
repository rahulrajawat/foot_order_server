var include = require('../../lib/include');
var KitchenSchema = include.KitchenSchema;

//=============================================================================================================
module.exports.addMenuItems = function (ResponseData, callback) {
	var where = {
		kitchen_id: ResponseData.kitchen_id,
		item_name: ResponseData.item_name
	};
	//console.log(where);

	if (ResponseData.category_id != null && ResponseData.category_id != undefined && ResponseData.category_id != "") {

		ResponseData.quantity = parseFloat(ResponseData.quantity);

		// include.KitchenSchema.crud(ResponseData, 1, where, (ErrorUpdateAccessToken, ResponseGet) => {
		// 	//console.log(ErrorUpdateAccessToken);

		// 	if (ErrorUpdateAccessToken.length <= 0) {
		include.KitchenSchema.crud(ResponseData, 2, null, (ErrorUpdateAccessToken1, ResponseInsertArray) => {
			//console.log(ErrorUpdateAccessToken1);
			callback(null, ErrorUpdateAccessToken1, 200);
		});
		// } else {
		// 	include.KitchenSchema.crud(ResponseData, 2, null, (ErrorUpdateAccessToken1, ResponseInsertArray) => {

		// 	callback([{ error: true }], null, 204);
		// }
		// });
	} else {
		callback({
			message: "Category_id is a required parameter",
			error: true
		}, null, 200);
	}
};

module.exports.updateMenuItems = function (ResponseData, callback) {
	console.log('updateMenuItems', ResponseData);
	var item_id = ResponseData.item_id
	var where = {
		_id: item_id
	};
	//console.log(where);
	delete ResponseData.item_id;
	// var updateData = {}
	// var reqKeyArray = Object.keys(ResponseData)
	// for (let i = 0; i < reqKeyArray.length; i++) {
	// 	// const element = array[i];
	// 	updateData[reqKeyArray[i]] = ResponseData[reqKeyArray[i]];

	// }
	/* var updateData = {
		calories: ResponseData.calories,
		carbohydrate: ResponseData.carbohydrate,
		carbohydrate_unit: ResponseData.carbohydrate_unit,
		category_id: ResponseData.category_id,
		description: ResponseData.description,
		fat: ResponseData.fat,
		fat_unit: ResponseData.fat_unit,
		item_name: ResponseData.item_name,
		price: ResponseData.price,
		protein: ResponseData.protein,
		Protein_unit: ResponseData.Protein_unit,
		quantity: ResponseData.quantity
	}; */

	include.KitchenSchema.crud(ResponseData, 3, where, (Success, Status) => {
		//console.log(ErrorUpdateAccessToken1);
		console.log(Success, Status);
		callback(null, Success, Status);
	});
};



//to do Search api
module.exports.searchFood = function (ResponseData, callback) {
	var postal_code = ResponseData.postal_code;
	var requirement = ResponseData.requirement;
	var price = ResponseData.price;
	var wherearr = [];
	var wherepostal = {};

	// include later when have all data

	if (postal_code != undefined && postal_code != null) {
		wherepostal = {
			Postal_Code: postal_code
		};
	}

	if (requirement != undefined && requirement != null && requirement != []) {
		var reqConArr = [];
		for (var i = 0; i < requirement.length; i++) {
			reqConArr.push({
				category_id: requirement[i]
			});
		}
		if (reqConArr.length > 0) {
			wherearr.push({
				$or: reqConArr
			});
		}
	}
	if (price != undefined && price != null && price != []) {
		var lt = 100,
			gt = 0;
		if (price.indexOf(',') > 0) {
			var csp = price.split(',');
			gt = parseFloat(csp[0]);
			lt = parseFloat(csp[1]);
		}
		wherearr.push({
			price: {
				$gte: gt,
				$lte: lt
			}
		});
	}

	console.log('where condtion', JSON.stringify(wherearr));
	// return;

	// //console.log(where);
	include.kitchenFeature_schema.crud(null, 1, wherepostal, (ResponseResult, StatusCode) => {
		//console.log("kitchenfeature",ResponseResult, StatusCode,wherepostal);
		
		// Here Response Check Send Response Code....
		if (ResponseResult  && ResponseResult.length > 0) {
			var kitchen_ids = [];
			var kitchenFeature = {};

			ResponseResult.forEach(function (element) {
				//console.log(element.kitchen_id);
				if (kitchen_ids.indexOf(element.kitchen_id) == -1) {
					kitchen_ids.push(element.kitchen_id);
					kitchenFeature[element.kitchen_id] = element;
				}
			}, this);
			wherearr.push({
				kitchen_id: {
					$in: kitchen_ids
				}
			});
			var where = {
				$and: wherearr
			};
			console.log(JSON.stringify(where));

			include.KitchenSchema.crud(null, 6, where, (kitchenResult, StatusCode) => {
				// //console.log(kitchenResult, StatusCode);
				var finalres = [];
				// //console.log("res",ResponseResult)
				if (kitchenResult.length > 0) {
					var menuItem = {};

					for (var index = 0; index < kitchenResult.length; index++) {
						var element = kitchenResult[index];
						// rowData.kitchenFeature = kitchenFeature[element.kitchen_id._id];
						if (menuItem[element.kitchen_id] == undefined) {
							menuItem[element.kitchen_id] = [];
						}
						menuItem[element.kitchen_id].push(kitchenResult[index]);
					}

					// //console.log('menuItem', JSON.stringify(kitchenResult));

					include.UserSchema.crud(null, 1, {
						$and: [{
							_id: {
								$in: kitchen_ids
							}
						}, {
							enabled: true
						}]

					}, (userResult, StatusCode) => {
						if (userResult.length > 0) {
							console.log(userResult.length,userResult);
							for (let r = 0; r < userResult.length; r++) {
								var userObj = userResult[r];
								var menuData = menuItem[userObj._id];
								if (menuData != null && menuData != undefined) {
									finalres.push({
										kitchenDetails: userObj,
										menuDetails: menuData,
										kitchenFeature: kitchenFeature[userObj._id]

										// description: userObj.description,
										// images: userObj.images,
										// price: userObj.price,
										// item_name: userObj.item_name
									});
								}
							}

							// //console.log('menuItem', JSON.stringify(finalres));
							// return;

							callback(null, finalres, 200);
						} else {
							callback([{
								error: true,
								message: 'No data found for your search'
							}], null, 500);
						}
					});
				} else {
					callback([{
						error: true,
						message: 'No data found for your search'
					}], null, 500);
				}
			});
		} else {
			callback([{
				error: true,
				message: 'No data found for your search'
			}], null, 500);
		}
	});
};

//=============================================================================================================

module.exports.deleteCardByUserId = function (ResponseData, callback) {
	var where = {
		user_id: ResponseData.user_id,
		_id: ResponseData.card_id
	};
	//console.log(where);
	include.KitchenSchema.crud(ResponseData, 4, where, (ErrorUpdateAccessToken, ResponseGet) => {
		if (ErrorUpdateAccessToken) {
			/*include.KitchenSchema.crud(ResponseData,2,null,(ErrorUpdateAccessToken1,ResponseInsertArray)=>{ */
			include.KitchenSchema.crud(
				ResponseData,
				1, {
					user_id: ResponseData.user_id
				},
				(ErrorUpdateAccessToken1, ResponseInsertArray) => {
					var temp = ErrorUpdateAccessToken1.find(function (element) {
						return element.is_default_payment_card == 1;
					});
					if (temp) {
						var MeResponse = {
							card: [{
								card_id: temp._id,
								is_default_payment_card: 1
							}]
						};
						callback(null, MeResponse, 200);
					} else {
						if (ErrorUpdateAccessToken1.length > 0) {
							include.KitchenSchema.crud({
									is_default_payment_card: 1
								},
								3, {
									user_id: ResponseData.user_id,
									_id: ErrorUpdateAccessToken1[0]._id
								},
								(ErrorUpdateAccessToken2, ResponseInsertArray) => {
									var MeResponse = {};

									if (ErrorUpdateAccessToken1.length > 0) {
										MeResponse = {
											card: [{
												card_id: ErrorUpdateAccessToken1[0]._id,
												is_default_payment_card: 1
											}]
										};
									}
									callback(null, MeResponse, 200);
								}
							);
						} else {
							// IF THereIS NO CARD REMAIN THID CONDTION
							callback(
								null, {
									card: []
								},
								200
							);
						}
					}
				}
			);
		} else {
			callback(null, [], 200);
		}
	});
};

//=============================================================================================================

module.exports.listMenuByKitchen = function (ResponseData, base_url, callback) {
	var where = {
		kitchen_id: ResponseData.kitchen_id
	};
	include.KitchenSchema.crud(ResponseData, 6, where, (ResponseResultArray, ResponseGet) => {
		if (ResponseResultArray.length > 0) {
			include.MasterCategorySchema.crud(null, 1, {}, (CategoryResponse, ResponseGet) => {
				if (CategoryResponse != null) {
					var category = {};
					for (var i = 0; i < CategoryResponse.length; i++) {
						category[CategoryResponse[i]._id] = CategoryResponse[i].category;
					}
					for (var j = 0; j < ResponseResultArray.length; j++) {
						ResponseResultArray[j].category_name = category[ResponseResultArray[j].category_id];
						// //console.log(ResponseResultArray[j].category_name);
					}
					// //console.log('if', ResponseResultArray);
					callback(null, ResponseResultArray, 200);
				} else {
					callback(null, ResponseResultArray, 200);
				}
			});
		} else {
			callback([{
				error: true
			}], [], 200);
		}
	});
};
//====================================================================================================================
module.exports.updateDefaultPaymentCard = function (ResponseData, base_url, callback) {
	var where = {
		user_id: ResponseData.user_id,
		_id: ResponseData.card_id
	};
	include.KitchenSchema.crud(null, 1, where, (ResponseGetIfExists, ResponseGetIfExistsStatus) => {
		var remove_status = 1;
		if (remove_status == 1) {
			var updateDefaultCard = {
				is_default_payment_card: 0
			};
			var updateWhere = {
				user_id: ResponseData.user_id,
				is_default_payment_card: 1
			};
			include.KitchenSchema.crud(updateDefaultCard, 3, updateWhere, (ResponseResultArray, ResponseGet) => {
				if (ResponseGet === 200) {
					//console.log(ResponseResultArray);
					// callback(null,[],200);
				}
			});
		}
		// Remove Status If Here End.....

		if (ResponseGetIfExists.length > 0) {
			// Update Here .....code..
			// Delete Here ....
			remove_status = 1;
			ResponseDataUpdated = {
				is_default_payment_card: 1
			};
			include.KitchenSchema.crud(ResponseDataUpdated, 3, where, (ResponseResultArray, ResponseGet) => {
				if (ResponseGet === 200) {
					//console.log(ResponseResultArray);
					//callback(null,[],200);
				}
			});
		} else {
			remove_status = 1;
			var ResponseDataUpdated = {
				user_id: ResponseData.user_id,
				_id: ResponseData.card_id,
				is_default_payment_card: 1
			};

			include.KitchenSchema.crud(ResponseDataUpdated, 2, where, (ResponseResultArray, ResponseGet) => {
				if (ResponseGet === 200) {
					//console.log(ResponseResultArray);
					//callback(null,[],200);
				}
			});
		}

		callback(null, [], 200);
	});

	//callback(null,[],200);
};

module.exports.deleteFoodItembyId = function (ResponseData, callback) {
	var {
		item_id
	} = ResponseData;
	include.OrderItem_schema.OrderItem_Details.find({
		item_id: item_id
	}).distinct('order_id', (error, order_ids) => {
		console.log("orderItm", order_ids)
		if (order_ids.length > 0) {
			include.Order_schema.crud(null, 1, {
				$and: [{
					_id: {
						$in: order_ids
					}
				}, {
					status: "pending"
				}]
			}, (err, orderData) => {
				if (orderData.length > 0) {
					callback([{
						error: true,
						message: "unable to delete this item as It's already associated to an order."
					}], null, 500)

				} else {
					deleteKitchen(item_id, (successdata, status) => {
						if (status == 200) {
							callback(null, [{
								error: false,
								message: "Successfully deleted the food item"
							}], status)
						} else {
							callback([{
								error: true,
								message: "Something went wrong please try again."
							}], null, status)

						}
					});

				}
			})

		} else {
			deleteKitchen(item_id, (successdata, status) => {
				if (status == 200) {
					callback(null, [{
						error: false,
						message: "Successfully deleted the food item"
					}], status)
				} else {
					callback([{
						error: true,
						message: "Something went wrong please try again."
					}], null, status)

				}
			});
		}
	})

};

var deleteKitchen = function (item_id, callback) {
	include.KitchenSchema.crud(null, 4, {
		_id: item_id
	}, (succ, status) => {
		console.log("delete", succ, status);
		callback(succ, status);
	})
}

module.exports.setDefaultImage = function (ResponseData, callback) {
	var {
		item_id,
		imagename
	} = ResponseData;

	include.KitchenSchema.crud(null, 1, {
		_id: item_id
	}, (kitchenData, status) => {
		if (kitchenData.length > 0) {
			if (kitchenData[0].images) {
				var updateData = new Object();
				var defIndex = kitchenData[0].images.indexOf(imagename)
				updateData.defaultImage = (defIndex != -1) ? defIndex : 0
				include.KitchenSchema.crud(updateData, 3, {
					_id: item_id
				}, (data, status) => {
					callback(null, [{
						error: false,
						message: imagename + " has been set as default image!"
					}], 200);
				})
			} else {
				callback([{
					error: true,
					message: "No image available"
				}], null, 500);
			}
		} else {
			callback([{
				error: true,
				message: "No item avaialable for this id! Please Refresh the page and try again."
			}], null, 500);
		}
	})

}

module.exports.deleteItemImage = function (ResponseData, callback) {
	var {
		item_id,
		imagename
	} = ResponseData;
	var fs = require('fs');
	// return;
	include.KitchenSchema.crud(null, 1, {
		_id: item_id
	}, (kitchenData, status) => {
		if (kitchenData.length > 0) {
			if (kitchenData[0].images) {

				var updateData = new Object();

				var filePath = __dirname.split("model")[0] + "images/" + imagename;
				console.log("imagepath=> ", filePath);
				if (fs.existsSync(filePath)) {
					fs.unlinkSync(filePath);
				}
				var defIndex = kitchenData[0].images.indexOf(imagename)

				console.log("before", kitchenData[0].images);
				kitchenData[0].images = kitchenData[0].images.filter(item => item !== imagename);
				console.log("after", kitchenData[0].images);

				updateData.images = kitchenData[0].images;


				updateData.defaultImage = (defIndex == kitchenData[0].defaultImage || kitchenData[0].images.length < kitchenData[0].defaultImage) ? 0 : kitchenData[0].defaultImage;

				include.KitchenSchema.crud(updateData, 3, {
					_id: item_id
				}, (data, status) => {
					callback(null, [{
						error: false,
						message: imagename + " has been deleted successfully!"
					}], 200);
				})
			} else {
				callback([{
					error: true,
					message: "No image available with this name."
				}], null, 500);
			}

		} else {
			callback([{
				error: true,
				message: "No item avaialable for this id! Please Refresh the page and try again."
			}], null, 500);
		}
	})

};

module.exports.getsubUrbs = function (param, callback) {

	include.countrySchema.crud(null, 1, {
		$or: [{
			locality: new RegExp('^' + param, "i")
		}, {
			postcode: new RegExp('^' + param, "i")
		}]
	}, (succ, status) => {
		// console.log("delete", succ, status);
		callback(null, succ, status);
	})
}
