const mongoose = require('mongoose');
// const { mongoose } = require('../../mongoconfig');

const Schema = mongoose.Schema,
	ObjectId = Schema.ObjectId;
// Here Defiend Schema For The Generate The Dynamicaly
/*
    @This is Generate The Dynamic Mongoose For the Dynamic Data
    @This is All Parameter Must Be Required,
    @password must be incrypted formate..
*/
const KitchenSchema = new Schema({
	item_name: {
		type: String,
		default: ''
	},
	category_id: {
		type: String,
		// default: "",
		ref: 'mastercategory'
	},
	price: {
		type: Number,
		default: 0
	},
	images: {
		type: Array,
		default: []
	},
	defaultImage: {
		type: Number,
		default: 0
	},
	description: {
		type: String,
		default: ''
	},
	kitchen_id: {
		type: String,
		// default: "",
		ref: 'users'
	},
	calories: {
		type: String,
		default: ''
	},
	quantity: {
		type: Number,
		default: 0
	},
	fat: {
		type: String,
		default: ''
	},
	fat_unit: {
		type: String,
		default: ''
	},
	carbohydrate: {
		type: String,
		default: ''
	},
	carbohydrate_unit: {
		type: String,
		default: ''
	},
	protein: {
		type: String,
		default: ''
	},
	Protein_unit: {
		type: String,
		default: ''
	},
	energy_280: {
		type: String,
		default: ''
	},
	energy_100: {
		type: String,
		default: ''
	},
	energy_unit: {
		type: String,
		default: ''
	},

	calorie_280: {
		type: String,
		default: ''
	},
	calorie_100: {
		type: String,
		default: ''
	},
	calorie_unit: {
		type: String,
		default: ''
	},

	fibre_280: {
		type: String,
		default: ''
	},
	fibre_100: {
		type: String,
		default: ''
	},
	fibre_unit: {
		type: String,
		default: ''
	},

	protein_280: {
		type: String,
		default: ''
	},
	protein_100: {
		type: String,
		default: ''
	},

	fat_280: {
		type: String,
		default: ''
	},
	fat_100: {
		type: String,
		default: ''
	},

	saturated_280: {
		type: String,
		default: ''
	},
	saturated_100: {
		type: String,
		default: ''
	},
	saturated_unit: {
		type: String,
		default: ''
	},

	carbohydrates_280: {
		type: String,
		default: ''
	},
	carbohydrates_100: {
		type: String,
		default: ''
	},
	carbohydrates_unit: {
		type: String,
		default: ''
	},

	sugars_280: {
		type: String,
		default: ''
	},
	sugars_100: {
		type: String,
		default: ''
	},
	sugars_unit: {
		type: String,
		default: ''
	},

	sodium_280: {
		type: String,
		default: ''
	},
	sodium_100: {
		type: String,
		default: ''
	},
	sodium_unit: {
		type: String,
		default: ''
	},

	createdAt: {
		type: Date,
		default: Date.now
	},
	updatedAt: {
		type: Date,
		default: Date.now
	},

	ingredients: {
		type: String,
		default: ''
	},
	allergens: {
		type: String,
		default: ''
	}
});
//Here Assign For The Mkae For The Globle Varible For The acess Data
let Kitchen = mongoose.model('kitchen', KitchenSchema);
module.exports.Kitchen = Kitchen;
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

module.exports.crud = function (DataArray, crudStatus, where, callback) {
	/*
      @ Globle Make For The Crud Operation
      @ Globle Make For Any where Accessble Here...
      @ Globle Make Data Must Be Partision
      # crudStatus =0 means Get All Data Here..
      # crudStatus =1 means Get Where Data
      # crudStatus =2 means Insert Data...
      # crudStatus =3 means update Data...
      # crudStatus =4 means delete Data...
    */
	if (crudStatus == 0) {
		Kitchen.find({}, function (ErrorResponse, ResponseArray) {
			if (ErrorResponse) {
				callback(null, 500);
			} else {
				callback(ResponseArray, 200);
			}
		});
	} else if (crudStatus == 1) {
		// End 0 Status Here ...
		// For The Data Get Where Condtion Here...
		Kitchen.find(where, function (ErrorResponse, ResponseArray) {
			if (ErrorResponse) {
				callback(null, 500);
			} else {
				callback(ResponseArray, 200);
			}
		});
	} else if (crudStatus == 2) {
		// End The Status crud Operation here ....1
		// For The Data Get Where Condtion Here...
		Kitchen.create(DataArray, function (ErrorResponse, ResponseArray) {
			if (ErrorResponse) {
				callback(null, 500);
			} else {
				callback(ResponseArray, 200);
			}
		});
	} else if (crudStatus == 3) {
		//End The Status Crud Code Here .....3
		// For The Data Get Where Condtion Here...
		Kitchen.findByIdAndUpdate(
			where._id, {
				$set: DataArray
			},
			function (ErrorResponse, ResponseArray) {
				if (ErrorResponse) {
					callback(null, 500);
				} else {
					callback(ResponseArray, 200);
				}
			}
		);
	} else if (crudStatus == 4) {
		// End Update Data Here......
		//End The Status Crud Code Here .....3
		// For The Data Get Where Condtion Here...
		Kitchen.find(where).remove(function (ErrorResponse, ResponseArray) {
			if (ErrorResponse) {
				callback(ErrorResponse, 500);
			} else {
				callback(ResponseArray, 200);
			}
		});
	} else if (crudStatus == 5) {
		//find one row
		// For The Data Get via Populate...

		Kitchen.find(where)
			.populate('kitchen_id')
			.populate('category_id')
			.exec(function (ErrorResponse, ResponseArray) {
				//console.log('err', ErrorResponse);
				if (ErrorResponse) {
					callback(null, 500);
				} else {
					callback(ResponseArray, 200);
				}
			});
	} else if (crudStatus == 6) {
		// For The Data Get via Populate...
		//console.log('Vishal');

		Kitchen.find(where)
			.populate('category_id')
			.exec(function (ErrorResponse, ResponseArray) {
				//console.log('Vishal', ErrorResponse);
				if (ErrorResponse) {
					callback(null, 500);
				} else {
					callback(ResponseArray, 200);
				}
			});
	} else if (crudStatus == 7) {
		// For The Data Get via Populate...
		//console.log('Vishal');

		Kitchen.findByIdAndUpdate(where, {
			$push: {
				images: DataArray
			}
		}, function (ErrorResponse, ResponseArray) {
			//console.log('Vishal', ErrorResponse);
			if (ErrorResponse) {
				callback(null, 500);
			} else {
				callback(ResponseArray, 200);
			}
		});
	} else {
		// End Delete Data Here......
		callback(true);
	}
};

//==================================================================================================