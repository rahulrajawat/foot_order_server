const mongoose = require('mongoose');
const Schema = mongoose.Schema,
	ObjectId = Schema.ObjectId;

const kitchenFeatureSchema = new Schema({
	kitchen_id: { type: 'String', default: '', ref: 'users' },
	Postal_Code: { type: 'String', default: '' },
	Delivery_Price: { type: 'String', default: '' },
	Monday: { types: 'Array', default: [] },
	Tuesday: { types: 'Array', default: [] },
	Wednesday: { types: 'Array', default: [] },
	Thursday: { types: 'Array', default: [] },
	Friday: { types: 'Array', default: [] },
	Saturday: { types: 'Array', default: [] },
	Sunday: { types: 'Array', default: [] },
	createdDate: { type: Date, default: Date.now },
	updatedDate: { type: Date, default: Date.now }
});
//Here Assign For The Mkae For The Globle Varible For The acess Data
let KitchenFeature = mongoose.model('kitchenFeature', kitchenFeatureSchema);
module.exports.KitchenFeature = KitchenFeature;

//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

module.exports.crud = function(DataArray, crudStatus, where, callback) {
	// //console.log('inschama', DataArray, crudStatus, where);
	/*
      @ Globle Make For The Crud Operation
      @ Globle Make For Any where Accessble Here...
      @ Globle Make Data Must Be Partision
      # crudStatus =0 means Get All Data Here..
      # crudStatus =1 means Get Where Data
      # crudStatus =2 means Insert Data...
      # crudStatus =3 means update Data...
      # crudStatus =4 means delete Data...
    */
	if (crudStatus == 0) {
		KitchenFeature.find({}, function(ErrorResponse, ResponseArray) {
			if (ErrorResponse) {
				callback(null, 500);
			} else {
				callback(ResponseArray, 200);
			}
		});
	} else if (crudStatus == 1) {
		// End 0 Status Here ...
		// For The Data Get Where Condtion Here...
		KitchenFeature.find(where, function(ErrorResponse, ResponseArray) {
			//console.log('inside shema', ErrorResponse, ResponseArray);
			if (ErrorResponse) {
				//console.log(ErrorResponse);
				callback(null, 500);
			} else {
				////console.log(ResponseArray);
				callback(ResponseArray, 200);
			}
		});
	} else if (crudStatus == 2) {
		// End The Status crud Operation here ....1
		// For The Data Get Where Condtion Here...
		KitchenFeature.collection.insert(DataArray, function(ErrorResponse, ResponseArray) {
			if (ErrorResponse) {
				callback(null, 500);
			} else {
				callback(ResponseArray, 200);
			}
		});
	} else if (crudStatus == 3) {
		//End The Status Crud Code Here .....3
		// For The Data Get Where Condtion Here...
		//console.log('kitchenFeatureSchema ', where, DataArray);
		KitchenFeature.update(where, { $set: DataArray }, function(ErrorResponse, ResponseArray) {
			if (ErrorResponse) {
				callback(null, 500);
			} else {
				callback(ResponseArray, 200);
			}
		});
	} else if (crudStatus == 4) {
		// End Update Data Here......
		//End The Status Crud Code Here .....3
		// For The Data Get Where Condtion Here...
		KitchenFeature.remove(where, function(ErrorResponse, ResponseArray) {
			if (ErrorResponse) {
				callback(null, 500);
			} else {
				callback(ResponseArray, 200);
			}
		});
	} else if (crudStatus == 5) {
		// For The Data Get via Populate...

		KitchenFeature.find(where)
			.populate('kitchen_id')
			.exec(function(ErrorResponse, ResponseArray) {
				//console.log('err', ErrorResponse);
				if (ErrorResponse) {
					callback(null, 500);
				} else {
					callback(ResponseArray, 200);
				}
			});
	}else if (crudStatus == 6) {
		// For The Data Get via Populate...

		KitchenFeature.distinct('Postal_Code', {}, function(error, postal_code) {
			// //console.log('vishal', error, postal_code);
			// Add a comment to this line
			if (error) {
				callback(null, 500);
			} else {
				callback(postal_code, 200);
			}
		});
	} else {
		// End Delete Data Here......
		callback(true);
	}
};

//==================================================================================================

module.exports.user_in = function(idsArray, callback) {
	KitchenFeature.find({ _id: { $in: idsArray } }, (ErrorMongo, ResponseMongo) => {
		if (ErrorMongo) {
			callback(ErrorMongo);
		} else {
			callback(ResponseMongo);
		}
	});
};
