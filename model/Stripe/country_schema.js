const mongoose = require('mongoose');
const Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

const SubUrbsschema = new Schema({
    "postcode": {
        type: String,
        default: ""
    },
    "locality": {
        type: String,
        default: ""
    },
    "state": {
        type: String,
        default: ""
    },
    "long": {
        type: String,
        default: ""
    },
    "lat": {
        type: String,
        default: ""
    },
    "dc": {
        type: String,
        default: ""
    },
    "type": {
        type: String,
        default: ""
    },
});
//Here Assign For The Mkae For The Globle Varible For The acess Data 
let SubUrbs = mongoose.model('SubUrbs_postalcodes', SubUrbsschema);
module.exports.SubUrbs = SubUrbs;



module.exports.crud = function (DataArray, crudStatus, where, callback) {
    /*
      @ Globle Make For The Crud Operation
      @ Globle Make For Any where Accessble Here...
      @ Globle Make Data Must Be Partision
      # crudStatus =0 means Get All Data Here..
      # crudStatus =1 means Get Where Data
      # crudStatus =2 means Insert Data...
      # crudStatus =3 means update Data...
      # crudStatus =4 means delete Data...
    */
    if (crudStatus == 0) {
        SubUrbs.find({}, function (ErrorResponse, ResponseArray) {
            if (ErrorResponse) {
                callback(null, 500);
            } else {
                callback(ResponseArray, 200);
            }
        });
    }
    // End 0 Status Here ...
    else if (crudStatus == 1) {
        // For The Data Get Where Condtion Here...
        SubUrbs.find(where, function (ErrorResponse, ResponseArray) {
            if (ErrorResponse) {
                callback(null, 500);
            } else {
                callback(ResponseArray, 200);
            }
        });
    }
    // End The Status crud Operation here ....1
    else if (crudStatus == 2) {
        // For The Data Get Where Condtion Here...
        SubUrbs.create(DataArray, function (ErrorResponse, ResponseArray) {
            //console.log(ErrorResponse);
            if (ErrorResponse) {
                callback(null, 500);
            } else {
                callback(ResponseArray, 200);
            }
        });

    }
    //End The Status Crud Code Here .....3
    else if (crudStatus == 3) {
        // For The Data Get Where Condtion Here...
        SubUrbs.update(where, {
            $set: DataArray
        }, function (ErrorResponse, ResponseArray) {
            if (ErrorResponse) {
                callback(null, 500);
            } else {
                callback(ResponseArray, 200);
            }
        });

    }

    // End Update Data Here......
    //End The Status Crud Code Here .....3
    else if (crudStatus == 4) {
        // For The Data Get Where Condtion Here...
        SubUrbs.remove(where, function (ErrorResponse, ResponseArray) {
            if (ErrorResponse) {
                callback(null, 500);
            } else {
                callback(ResponseArray, 200);
            }
        });

    }

    // End Delete Data Here......
    else {
        callback(true);
    }

}

//==================================================================================================