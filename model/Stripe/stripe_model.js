var include = require('../../lib/include');
var stripe = include.stripekey;
// for live payments uncomment this line
//var stripe = include.stripekeylive;

var fs = require('fs');

module.exports.subscribeUserStripe = function (ResponseData, host, callback) {
	stripe.accounts
		.create({
			country: ResponseData.country,
			email: ResponseData.email,
			type: 'custom'
			// display_name:ResponseData.kitchen_name,
			// business_name: ResponseData.kitchen_name
		})
		.then(function (acct) {
			console.log(host);
			stripe.accounts.update(acct.id, {
				tos_acceptance: {
					date: Math.floor(Date.now() / 1000),
					ip: host // Assumes you're not using a proxy
				},
				business_name: ResponseData.kitchen_name,

			});

			callback(acct);
		});
	//     stripe.countrySpecs.list({
	//             limit: 3
	//         },
	//         function (err, countrySpecs) {
	//             // asynchronously called
	// callback(countrySpecs);
	//         }
	//     );
};
module.exports.addBankAccStripe = function (ResponseData, callback) {
	// console.log({_id:ResponseData.kitchen_id});
	include.UserSchema.crud(null, 1, {
		_id: ResponseData.kitchen_id
	}, (userData, statusCode) => {
		// console.log(userData,statusCode);
		var stripe_token = userData[0].stripe_token;
		var bankAcObj = {
			object: 'bank_account',
			account_number: ResponseData.acc_no,
			country: ResponseData.country_code,
			currency: ResponseData.currency,
			account_holder_name: ResponseData.username,
			routing_number: ResponseData.routing_number
		};
		var dobObj = {}
		if (ResponseData.dob) {
			var dob = ResponseData.dob.split("-");
			dobObj = {
				day: dob[0],
				month: dob[1],
				year: dob[2]
			}
		}
		console.log(bankAcObj, dobObj);
		stripe.accounts.update(stripe_token, {
			external_account: bankAcObj,
			legal_entity: {
				business_name: userData[0].kitchen_name,
				type: 'company',
				dob: dobObj,
				first_name: ResponseData.first_name,
				last_name: ResponseData.last_name
			}
		}, function (
			err,
			bank_account
		) {
			callback(bank_account, err);
		});
	});
};
module.exports.countryimport = function (callback) {
	var obj;
	fs.readFile('./australian_postcodes.json', 'utf8', function (err, data) {
		if (err) throw err;
		obj = JSON.parse(data);
		include.countrySchema.SubUrbs.collection.insert(obj, (success, err) => {
			console.log(success, err);
			callback(success, err);
		});
	});
};

module.exports.paymentSplitStripe = function (ResponseData, order_id, user_stripe_token, callback) {
	var quantity = ResponseData.quantity,
		user_id = ResponseData.user_id,
		// order_status = ResponseData.order_status,
		deliverydate = ResponseData.deliverydate,
		amount = parseFloat(ResponseData.amount),
		currency = ResponseData.currency,
		kitchen_id = ResponseData.kitchen_id,
		delivery_price = parseFloat(ResponseData.delivery_price),
		admin_id = '59f4953fc3b84a11a0cdd534';
	// total amount + deliver;
	var total = parseFloat((amount + delivery_price).toFixed(2));
	// var total = amount + 15;

	// stripe charges 1.75% +30cents
	// 7% + GST HH commission GST 10% on the commission
	var admincommission = amount * 0.07 + 0.1 * (amount * 7 / 100);
	// adminShare = commission - stripecharges;
	var stripeFee = (total * (1.75 / 100)) + 0.3;
	var vendorShare = total - admincommission;

	console.log('amount=>' + total);
	console.log('admincommission=>' + admincommission);
	console.log('vendorShare=>' + vendorShare);
	include.UserSchema.crud(null, 1, {
		_id: kitchen_id
	}, (kitchenResponse, kitchenStat) => {

		stripe.charges.create({
				amount: (parseFloat(total) * 100),
				currency: currency,
				source: user_stripe_token, // obtained with Stripe.js
				description: 'Charge of amount ' + total + ' ' + currency + ' created for ' + order_id,
				metadata: {
					total: 'total amount = ' + total,
					price: "actual price of product => " + amount,
					delivery_price: "Delivery price of product => " + delivery_price,
					vendorshare: "Vendor's share =" + vendorShare,
					admincommission: 'admin commission (share) = ' + /* Math.round( */ (admincommission - stripeFee) /* ) */ ,
					adminRoundof: 'admin commission (share)(after rounding of) = ' + Math.round((admincommission - stripeFee)),
					stripe_charges: 'Stripe Fee for this = ' + (stripeFee),
					note: "The stripe fee might differ in case of AUD then our calcuations..."
				},
				application_fee: Math.round((admincommission - stripeFee) * 100),
			},
			// uncomment this line to create direct charge to connected account npt tested but can cost multiple transaction charges that's why using connect transfer utility
			{
				stripe_account: kitchenResponse[0].stripe_token
			},

			function (err, charge) {
				if (err == null) {
					console.log('charge created', charge);
					var chargeData = {
						id: charge.id,
						trnsx_id: charge.balance_transaction,
						amount: charge.amount,
						currency: charge.currency,
						description: charge.description,
						status: charge.status,
						order_id: order_id,
						user_id: user_id
					};
					include.Payment_schema.crud(chargeData, 2, null, function (orderUpdRes, orderUpdStat) {
						include.Order_schema.crud({
							trnsx_id: charge.id
						}, 3, {
							_id: order_id
						}, function (
							orderUpdRes,
							orderUpdStat
						) {
							console.log('order status update ', orderUpdRes);
							// console.log('Kitchen details ', kitchenResponse[0]);
							var transferData = {
								id: charge.id,
								trnsx_id: charge.balance_transaction,
								amount: admincommission,
								currency: charge.currency,
								description: 'Commission paid to admin',
								status: charge.status,
								order_id: order_id,
								user_id: admin_id
							};
							include.Payment_schema.crud(transferData, 2, null, function (
								orderUpdRes,
								orderUpdStat
							) {});

							console.log('last update');
							callback(
								null, [{
									error: false,
									message: 'Order added successfully',
									trnsx_id: charge.id
								}],
								200
							);

						});
					});
				} else {
					callback(err, null, 500);
				}
			}
		);
	});

};


module.exports.uploadLegalDoc = function (ResponseData, filename, callback) {
	// console.log(ResponseData);
	include.UserSchema.crud(null, 1, {
		_id: ResponseData.kitchen_id
	}, (kitchenData, statusCode) => {
		// console.log(kitchenData, statusCode);
		if (kitchenData.length > 0) {

			var filepath = __dirname.split("model")[0] + "/images/docs/" + filename;
			stripe.fileUploads.create({
				purpose: 'identity_document',
				file: {
					data: fs.readFileSync(filepath),
					name: filename,
					type: 'application/octet-stream'
				}
			}, {
				stripe_account: kitchenData[0].stripe_token
			}, function (err, success) {
				console.log(err, success)
				if (err) {

					callback([{
						error: true,
						message: "something went wrong"
					}], err);
				} else {
					callback(err, [{
						error: false,
						message: "Success"
					}]);
				}
			});
		} else {
			callback([{
				error: true,
				message: "No kitchen found with this name"
			}], null);

		}
	})
}

module.exports.getListedAcc = function (ResponseData, callback) {
	include.UserSchema.crud(null, 1, {
		_id: ResponseData.kitchen_id
	}, (kitchen_data, status) => {
		if (kitchen_data.length > 0) {

			stripe.accounts.listExternalAccounts(kitchen_data[0].stripe_token, {
				object: "bank_account"
			}, function (err, bank_accounts) {
				callback(null, bank_accounts, 200)
			});
		} else {
			callback([{
				error: true,
				message: "No kitchen found with this name"
			}], null, 500);

		}
	})
}