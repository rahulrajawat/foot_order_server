const mongoose = require('mongoose');
const Schema = mongoose.Schema,
ObjectId = Schema.ObjectId;
// Here Defiend Schema For The Generate The Dynamicaly
/*
@This is Generate The Dynamic Mongoose For the Dynamic Data
@This is All Parameter Must Be Required,
@password must be incrypted formate..
*/
const TrnsxSchema = new Schema({
    sender_id : {type:String, default: ""},
    reciever_id : {type:String, default: ""},
    trnsx_id: {type:String, default: ""},
    date : {type:Date, default: new Date() },
    status : {type:String, default: ""},
    currency : {type:String, default: ""},
    amount : {type:String, default: ""},
    trnsx_type : {type:String, default: ""},
    updatedDate : {type:Date, default: new Date()}
});
//Here Assign For The Mkae For The Globle Varible For The acess Data 
let Trnsx = mongoose.model('trnsx_details', TrnsxSchema);
module.exports.trnsx = Trnsx;
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


module.exports.crud = function(DataArray, crudStatus, where, callback) {
    /*
    @ Globle Make For The Crud Operation
    @ Globle Make For Any where Accessble Here...
    @ Globle Make Data Must Be Partision
    # crudStatus =0 means Get All Data Here..
    # crudStatus =1 means Get Where Data
    # crudStatus =2 means Insert Data...
    # crudStatus =3 means update Data...
    # crudStatus =4 means delete Data...
    */
    if (crudStatus == 0) {
        Trnsx.find({}, function(ErrorResponse, ResponseArray) {
            if (ErrorResponse) {
                callback(null, 500);
            } else {
                callback(ResponseArray, 200);
            }
        });
    }
    // End 0 Status Here ...
    else if (crudStatus == 1) {
        // For The Data Get Where Condtion Here...
        Trnsx.find(where, function(ErrorResponse, ResponseArray) {
            if (ErrorResponse) {
                callback(null, 500);
            } else {
                callback(ResponseArray, 200);
            }
        });
    }
    // End The Status crud Operation here ....1
    else if (crudStatus == 2) {
        // For The Data Get Where Condtion Here..
        Trnsx.create(DataArray, function(ErrorResponse, ResponseArray) {
            if (ErrorResponse) {
                callback(null, 500);
            } else {
                callback(ResponseArray, 200);
            }
        });

    }
    //End The Status Crud Code Here .....3
    else if (crudStatus == 3) {
        // For The Data Get Where Condtion Here...
        Trnsx.update(where, {
            $set: DataArray
        }, function(ErrorResponse, ResponseArray) {
            if (ErrorResponse) {
                callback(null, 500);
            } else {
                callback(ResponseArray, 200);
            }
        });

    }

    // End Update Data Here......
    //End The Status Crud Code Here .....3
    else if (crudStatus == 4) {
        // For The Data Get Where Condtion Here...
        Trnsx.remove(where, function(ErrorResponse, ResponseArray) {
            if (ErrorResponse) {
                callback(null, 500);
            } else {
                callback(ResponseArray, 200);
            }
        });

    }

    // End Delete Data Here......
    else {
        callback(true);
    }

}

//==================================================================================================