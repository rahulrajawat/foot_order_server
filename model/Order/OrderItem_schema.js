const mongoose = require('mongoose');
const Schema = mongoose.Schema,
	ObjectId = Schema.ObjectId;
// Here Defiend Schema For The Generate The Dynamicaly
/*
     @This is Generate The Dynamic Mongoose For the Dynamic Data
     @This is All Parameter Must Be Required,
     @password must be incrypted formate..
 */
const OrderItem_Schema = new Schema({
	item_id: { type: String, default: '', ref: 'kitchen' },
	order_id: { type: String, default: '', ref: 'OrderDetails' },
	quantity: { type: String, default: '' },	
	// name: { type: String, default: '' },
	updatedAt: { type: Date, default: Date.now }
});
//Here Assign For The Mkae For The Globle Varible For The acess Data
let OrderItem_Details = mongoose.model('OrderItem_Details', OrderItem_Schema);
module.exports.OrderItem_Details = OrderItem_Details;

//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

module.exports.crud = function(DataArray, crudStatus, where, callback) {
	/*
      @ Globle Make For The Crud Operation
      @ Globle Make For Any where Accessble Here...
      @ Globle Make Data Must Be Partision
      # crudStatus =0 means Get All Data Here..
      # crudStatus =1 means Get Where Data
      # crudStatus =2 means Insert Data...
      # crudStatus =3 means update Data...
      # crudStatus =4 means delete Data...
    */
	if (crudStatus == 0) {
		OrderItem_Details.find({}, function(ErrorResponse, ResponseArray) {
			if (ErrorResponse) {
				callback(null, 500);
			} else {
				callback(ResponseArray, 200);
			}
		});
	} else if (crudStatus == 1) {
		// End 0 Status Here ...
		// For The Data Get Where Condtion Here...
		OrderItem_Details.find(where, function(ErrorResponse, ResponseArray) {
			if (ErrorResponse) {
				//console.log(ErrorResponse);
				callback(null, 500);
			} else {
				////console.log(ResponseArray);
				callback(ResponseArray, 200);
			}
		});
	} else if (crudStatus == 2) {
		// End The Status crud Operation here ....1
		// For The Data Get Where Condtion Here...
		OrderItem_Details.collection.insert(DataArray, function(ErrorResponse, ResponseArray) {
			if (ErrorResponse) {
				callback(null, 500);
			} else {
				callback(ResponseArray, 200);
			}
		});
	} else if (crudStatus == 3) {
		//End The Status Crud Code Here .....3
		// For The Data Get Where Condtion Here...
		//console.log('OrderItem_DetailssSchema ', where, DataArray);
		OrderItem_Details.update(where, { $set: DataArray }, function(ErrorResponse, ResponseArray) {
			if (ErrorResponse) {
				callback(null, 500);
			} else {
				callback(ResponseArray, 200);
			}
		});
	} else if (crudStatus == 4) {
		// End Update Data Here......
		//End The Status Crud Code Here .....3
		// For The Data Get Where Condtion Here...
		OrderItem_Details.remove(where, function(ErrorResponse, ResponseArray) {
			if (ErrorResponse) {
				callback(null, 500);
			} else {
				callback(ResponseArray, 200);
			}
		});
	} else if (crudStatus == 5) {
		// End Update Data Here......
		//End The Status Crud Code Here .....3
		// For The Data Get Where Condtion Here...
		OrderItem_Details.find(where)
			// .populate({path: 'order_id',populate:{path:'kitchen_id user_id'}})
			.populate({path: 'item_id',populate:{path:'category_id'}})
				.exec(function(ErrorResponse, ResponseArray) {
				// console.log(ErrorResponse, ResponseArray);
				if (ErrorResponse!=null) {
					callback(null, 500);
				} else {
					callback(ResponseArray, 200);
				}
			});
	} else if (crudStatus == 6) {
		// End Update Data Here......
		//End The Status Crud Code Here .....3
		// For The Data Get Where Condtion Here...
		OrderItem_Details.find(where)
			.populate({path: 'item_id',populate:{path:'category_id'}})
				.exec(function(ErrorResponse, ResponseArray) {
				// console.log(ErrorResponse, ResponseArray);
				if (ErrorResponse!=null) {
					callback(null, 500);
				} else {
					callback(ResponseArray, 200);
				}
			});
	} else {
		// End Delete Data Here......
		callback(true);
	}
};

//==================================================================================================

module.exports.user_in = function(idsArray, callback) {
	OrderItem_Details.find({ _id: { $in: idsArray } }, (ErrorMongo, ResponseMongo) => {
		if (ErrorMongo) {
			callback(ErrorMongo);
		} else {
			callback(ResponseMongo);
		}
	});
};

//==================================================================================*************============================================
