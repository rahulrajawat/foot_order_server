var include = require('../../lib/include');
var stripe = include.stripekey;
// var mongoose = require('mongoose');

//=========================================================================================================
module.exports.createOrder = function (ResponseData, callback) {
	console.log('createOrder ', ResponseData);
	var item = ResponseData.item,
		quantity = ResponseData.quantity,
		user_id = ResponseData.user_id,
		delivery_price = parseFloat(ResponseData.delivery_price),
		deliverydate = ResponseData.deliverydate,
		amount = parseFloat(ResponseData.amount),
		currency = ResponseData.currency,
		kitchen_id = ResponseData.kitchen_id,
		delivery_address = ResponseData.delivery_address,
		other_info = ResponseData.other_info,
		secure_building = ResponseData.secure_building,
		authority = ResponseData.authority,
		user_stripe_token = ResponseData.stripe_token;
	ResponseData.createdAt = Date.now();

	include.EndUserSchema.crud({
		stripe_token: user_stripe_token
	}, 3, {
		_id: user_id
	}, function (
		userupdate,
		userUpdStatus
	) {
		include.EndUserSchema.crud(null, 1, {
			_id: user_id
		}, (userDetail, userstattt) => {


			if (userUpdStatus == 200) {
				delete ResponseData.stripe_token;
				delete ResponseData.item;
				include.Order_schema.crud(ResponseData, 2, null, function (orderInsRes, orderInsStat) {
					console.log('order res ', orderInsRes, orderInsStat);
					if (orderInsRes.length > 0) {
						for (let index = 0; index < item.length; index++) {
							const element = item[index];

							include.KitchenSchema.crud(null, 1, {
								_id: element.item_id
							}, function (
								MenuItemsRes,
								MenuItemsResStatus
							) {
								if (parseFloat(MenuItemsRes[0].quantity) > parseFloat(element.quantity)) {
									include.KitchenSchema.crud({
											quantity: parseFloat(MenuItemsRes[0].quantity) - parseFloat(element.quantity)
										},
										3, {
											_id: element.item_id
										},
										function (ktchenUpdRes, kitchenUpdStatus) {
											console.log(element.name + ' =>', ktchenUpdRes);
										}
									);
								} else {
									callback(
										[{
											error: true,
											message: 'insuffecient quantity please decrease ' + element.name + "'s Quantity."
										}],
										null,
										500
									);
									return;
								}
							});
						}
						var order_id = '' + orderInsRes[0]._id;
						// var orderItems = [];
						for (let i = 0; i < item.length; i++) {
							item[i].order_id = order_id;
							// orderItems.push({ item_id: item_id[i], order_id: order_id });
						}
						include.OrderItem_schema.crud(item, 2, null, function (itemInsert, itemInsertStat) {
							if (itemInsertStat == 200) {
								include.stripe_model.paymentSplitStripe(
									ResponseData,
									order_id,
									user_stripe_token,
									(error, successdata, status) => {
										if (error == null) {
											include.OrderItem_schema.crud(null, 6, {
												order_id: order_id
											}, (orderItems, oistat) => {
												let tblerow = '',
													total = 0
												orderItems.forEach((oi, i) => {
													console.log("orderitem ", oi);

													tblerow += `
												<tr>
													<td style="word-wrap:break-word;white-space:normal">
														<div style="margin-top:10px">${oi.item_id.item_name} </div>
													</td>
													<td>
														<div style="float:right;padding-top:10px">${oi.quantity}</div>
													</td>
													<td>
														<div style="float:right;padding-top:10px">${currency} ${oi.item_id.price}</div>
													</td>
													<td>
														<div style="float:right;padding-top:10px">${parseFloat(oi.quantity) * parseFloat(oi.item_id.price)}</div>
													</td>
												</tr>`
													total += parseFloat(oi.quantity) * parseFloat(oi.item_id.price)
													if (i == orderItems.length - 1) {
														tblerow += `
													<tr>
													<td></td>
													<td class="m_-4904054520529085693m_5712782259546021053m_-3523226219421901595orderAmounts"
														style="padding-top:20px;padding-bottom:10px;height:40px">
														<div class="m_-4904054520529085693m_5712782259546021053m_-3523226219421901595float-right" style="text-align:right">Sub total</div>
													</td>
													<td class="m_-4904054520529085693m_5712782259546021053m_-3523226219421901595orderAmounts"
														style="padding-top:20px;padding-bottom:10px;height:40px"
														colspan="2">
														<div class="m_-4904054520529085693m_5712782259546021053m_-3523226219421901595float-right"
															style="text-align:right">
															${currency} ${total}</div>
													</td>
												</tr>
												<tr>
													<td></td>
													<td class="m_-4904054520529085693m_5712782259546021053m_-3523226219421901595orderAmounts"
														style="height:40px">
														<div class="m_-4904054520529085693m_5712782259546021053m_-3523226219421901595float-right"
															style="text-align:right">Delivery
															Charges</div>
													</td>
													<td class="m_-4904054520529085693m_5712782259546021053m_-3523226219421901595orderAmounts"
														style="height:40px"
														colspan="2">
														<div class="m_-4904054520529085693m_5712782259546021053m_-3523226219421901595float-right"
															style="text-align:right">${currency} ${delivery_price}</div>
													</td>
												</tr>
												<tr>
													<td></td>
													<td class="m_-4904054520529085693m_5712782259546021053m_-3523226219421901595orderAmounts"
														style="height:40px">
														<div class="m_-4904054520529085693m_5712782259546021053m_-3523226219421901595float-right m_-4904054520529085693m_5712782259546021053m_-3523226219421901595font-bold"
															style="font-weight:bold;text-align:right">Total
															Order
															Amount</div>
													</td>
													<td class="m_-4904054520529085693m_5712782259546021053m_-3523226219421901595orderAmounts"
														style="height:40px"
														colspan="2">
														<div class="m_-4904054520529085693m_5712782259546021053m_-3523226219421901595float-right m_-4904054520529085693m_5712782259546021053m_-3523226219421901595font-bold"
															style="font-weight:bold;text-align:right">${currency} ${delivery_price+total}</div>
													</td>
												</tr>`
													}
												});
												include.UserSchema.crud(null, 1, {
													_id: kitchen_id
												}, (kitchendetail, st) => {

													let localData = {
														orderItems: tblerow,
														order_id: order_id,
														order_amount: delivery_price + amount,
														kitchen_name: kitchendetail[0].kitchen_name,
														kitchen_contact: "+" + kitchendetail[0].country_code + " " + kitchendetail[0].phone_no,
														username: userDetail[0].full_name,
														user_add: delivery_address.address_1 + " " + delivery_address.address_2 + " " + delivery_address.city + " " + delivery_address.state + " " + delivery_address.country,
														user_phone: "+" + userDetail[0].country_code + " " + userDetail[0].contact,
														order_date: new Date(),
														deliverydate: deliverydate,
														secure_building: secure_building.secure_building,
														authority: authority.authority,
														other_info: other_info,
													}

													console.log("b4 mail", localData);
													// return


													include.utils.sendEmail(userDetail[0].email,
															"Order Placed",
															"orderconfirmation",
															userDetail[0].full_name, localData)
														.then((res) => console.log("mail sent"))
														.catch(err => console.log("mail error", err))

													console.log('last step');
													console.log(error, successdata, status);
													callback(error, successdata, status);
												})
											})
										} else {
											callback([{
												error: true,
												message: error.Error
											}], null, 500);
										}

									}
								);
							} else {
								callback([{
									error: true,
									message: 'Items insert error'
								}], null, 500);
							}
						});
					} else {
						callback([{
							error: true,
							message: 'order insert error'
						}], null, 500);
					}
				});
			} else {
				callback([{
					error: true,
					message: 'userudate failed'
				}], null, 500);
			}
		})
	});

};

module.exports.listOrder = function (ResponseData, callback) {
	var id = ResponseData.id,
		user_type = ResponseData.user_type;

	var where = {};
	if (user_type == 'kitchen') {
		where = {
			kitchen_id: id
		};
	} else if (user_type == 'admin') {
		where = {};
	} else {
		where = {
			user_id: id
		};
	}

	console.log(where);
	include.Order_schema.crud(null, 5, where, function (orderData, orderDStat) {
		if (orderData.length > 0) {
			var orderIds = [];

			orderData.forEach((element) => {
				orderIds.push(element._id);
			});

			where = {
				order_id: {
					$in: orderIds
				}
			};

			// console.log(where);
			include.OrderItem_schema.crud(null, 5, where, function (orderRes, orderStat) {
				// console.log(orderRes, orderStat);
				if (orderStat == 200) {
					var itemRes = [],
						finalRes = [];
					var itemObj = {};
					orderRes.forEach((itemElement) => {
						// console.log(itemElement.order_id == orderElement._id);
						// itemObj = orderElement;
						if (itemObj[itemElement.order_id] == undefined) {
							itemObj[itemElement.order_id] = [];
						}
						// console.log(itemObj[itemElement.order_id]);
						itemObj[itemElement.order_id].push(itemElement);
					});
					// console.log(itemObj);;
					for (let index = 0; index < orderData.length; index++) {
						const orderElement = orderData[index];

						var orderObj = {};
						if (itemObj[orderElement._id] != undefined && orderElement.trnsx_id != "") {
							finalRes.push({
								order_id: orderElement._id,
								user: orderElement.user_id,
								deliverydate: orderElement.deliverydate,
								delivery_address: orderElement.delivery_address,
								other_info: orderElement.other_info,
								secure_building: orderElement.secure_building,
								authority: orderElement.authority,
								amount: orderElement.amount,
								kitchen: orderElement.kitchen_id,
								currency: orderElement.currency,
								items: itemObj[orderElement._id],
								order_status: orderElement.order_status,
								trnsx_id: orderElement.trnsx_id,
								delivery_price: orderElement.delivery_price,
							});
						}
					}

					callback(null, finalRes, 200);
				} else {
					callback([{
						error: true,
						message: 'No data found'
					}], null, 200);
				}
			});
		} else {
			callback([{
				error: true,
				message: 'No data found'
			}], null, 200);
		}
	});
};

module.exports.getOrderbyId = function (ResponseData, callback) {
	var order_id = ResponseData.order_id;
	// user_type = ResponseData.user_type;

	var where = {
		_id: order_id
	};
	console.log(where);
	include.Order_schema.crud(null, 5, where, function (orderData, orderDStat) {
		if (orderData.length > 0) {
			var orderIds = [];

			where = {
				order_id: order_id
			};

			console.log(where);

			include.OrderItem_schema.crud(null, 6, where, function (orderRes, orderStat) {
				// console.log(orderRes, orderStat);
				var finalOrder = orderData;
				if (orderStat == 200) {
					callback(
						null,
						[{
							trnsx_id: orderData[0].trnsx_id,
							authority: orderData[0].authority,
							secure_building: orderData[0].secure_building,
							currency: orderData[0].currency,
							amount: orderData[0].amount,
							delivery_price: orderData[0].delivery_price,
							kitchendetail: orderData[0].kitchen_id,
							other_info: orderData[0].other_info,
							delivery_address: orderData[0].delivery_address,
							deliverydate: orderData[0].deliverydate,
							order_status: orderData[0].order_status,
							userDetail: orderData[0].user_id,
							items: orderRes,
							createdAt: orderData[0].createdAt,
							updatedAt: orderData[0].updatedAt
						}],
						200
					);
				} else {
					callback([{
						error: true,
						message: 'No data found'
					}], null, 200);
				}
			});
		} else {
			callback([{
				error: true,
				message: 'No data found'
			}], null, 200);
		}
	});
};


module.exports.updateOrderStatus = function (requestData, callback) {
	console.log('requestData ', requestData);
	var inputData = {
			order_status: requestData.order_status
		},
		where = {
			_id: requestData.order_id
		};


	include.Order_schema.crud(inputData, 3, where, function (
		updates,
		updateStatus
	) {
		if (updateStatus == 200) {
			callback(null, updates, updateStatus);

		} else {
			callback([{
				error: true,
				message: 'update failed'
			}], null, 500);
		}
	});

};