const mongoose = require('mongoose');
const Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;
 // Here Defiend Schema For The Generate The Dynamicaly
 /*
     @This is Generate The Dynamic Mongoose For the Dynamic Data
     @This is All Parameter Must Be Required,
     @password must be incrypted formate..
 */
    const OrderSchema = new Schema({
        // '_id':{type: Schema.ObjectId, ref:'OrderItem_Details'},
        'quantity':{type: Number, default: 0},
        'user_id':{type: String, default: '',ref:'end_users'},
        'order_status':{type: String, default: 'pending'},
        'deliverydate':{type: String, default: ''},
        'delivery_address':{type: Object, default: {}},
        'other_info':{type: String, default: ''},
        'kitchen_id':{type: String, default: '',ref:'users'},
		'amount':{type: Number, default: 0},
		'delivery_price':{type: Number, default: 0},
        'currency':{type: String, default: ""},
        'secure_building':{type: Object, default: {}},
        'authority':{type: Object, default: {}},
        'createdAt':{type: Date, default: null},
		'updatedAt':{type: Date, default: null},
		'trnsx_id':{type: String, default: ""}
    });
  //Here Assign For The Mkae For The Globle Varible For The acess Data 
  let OrderDetails = mongoose.model('OrderDetails',OrderSchema);
  module.exports.OrderDetails =OrderDetails;

//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

module.exports.crud = function(DataArray, crudStatus, where, callback) {
	/*
      @ Globle Make For The Crud Operation
      @ Globle Make For Any where Accessble Here...
      @ Globle Make Data Must Be Partision
      # crudStatus =0 means Get All Data Here..
      # crudStatus =1 means Get Where Data
      # crudStatus =2 means Insert Data...
      # crudStatus =3 means update Data...
      # crudStatus =4 means delete Data...
    */
	if (crudStatus == 0) {
		OrderDetails.find({}, function(ErrorResponse, ResponseArray) {
			if (ErrorResponse!=null) {
				callback(null, 500);
			} else {
				callback(ResponseArray, 200);
			}
		});
	} else if (crudStatus == 1) {
		// End 0 Status Here ...
		// For The Data Get Where Condtion Here...
		OrderDetails.find(where, function(ErrorResponse, ResponseArray) {
			if (ErrorResponse!=null) {
				//console.log(ErrorResponse);
				callback(null, 500);
			} else {
				////console.log(ResponseArray);
				callback(ResponseArray, 200);
			}
		});
	} else if (crudStatus == 2) {
		// End The Status crud Operation here ....1
		// For The Data Get Where Condtion Here...
		OrderDetails.collection.insert(DataArray, function(ErrorResponse, ResponseArray) {
			// console.log(ErrorResponse, ResponseArray)
			if (ErrorResponse!=null) {
				callback(null, 500);
			} else {
				callback(ResponseArray.ops, 200);
			}
		});
	} else if (crudStatus == 3) {
		//End The Status Crud Code Here .....3
		// For The Data Get Where Condtion Here...
		//console.log('OrderDetailssSchema ', where, DataArray);
		OrderDetails.update(where, { $set: DataArray }, function(ErrorResponse, ResponseArray) {
			if (ErrorResponse!=null) {
				callback(null, 500);
			} else {
				callback(ResponseArray, 200);
			}
		});
	} else if (crudStatus == 4) {
		// End Update Data Here......
		//End The Status Crud Code Here .....3
		// For The Data Get Where Condtion Here...
		OrderDetails.remove(where, function(ErrorResponse, ResponseArray) {
			if (ErrorResponse!=null) {
				callback(null, 500);
			} else {
				callback(ResponseArray, 200);
			}
		});
	} else if (crudStatus == 5) {

		OrderDetails.find(where)
		.populate({path:'kitchen_id user_id'})
		// .populate('user_id')
		.exec(function(ErrorResponse, ResponseArray) {
			// console.log(ErrorResponse, ResponseArray);
			if (ErrorResponse!=null) {
				callback(null, 500);
			} else {
				callback(ResponseArray, 200);
			}
		});
		
	} else {
		// End Delete Data Here......
		callback(true);
	}
};

//==================================================================================================

module.exports.user_in = function(idsArray, callback) {
	OrderDetails.find({ _id: { $in: idsArray } }, (ErrorMongo, ResponseMongo) => {
		if (ErrorMongo) {
			callback(ErrorMongo);
		} else {
			callback(ResponseMongo);
		}
	});
};

  //==================================================================================*************============================================

  