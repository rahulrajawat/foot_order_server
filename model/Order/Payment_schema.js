const mongoose = require('mongoose');
const Schema = mongoose.Schema,
	ObjectId = Schema.ObjectId;
// Here Defiend Schema For The Generate The Dynamicaly
/*
     @This is Generate The Dynamic Mongoose For the Dynamic Data
     @This is All Parameter Must Be Required,
     @password must be incrypted formate..
 */
const Payment_Schema = new Schema({
	id: { type: String, default: '' },
	trnsx_id: { type: String, default: '' },
	amount: { type: Number },
	currency: { type: String, default: '' },
	description: { type: String, default: '' },
	status: { type: String, default: '' },
	order_id: { type: String, default: '', ref: 'OrderDetails' },
	user_id: { type: String, default: '', ref: 'end_users' },
	createdAt: { type: Date, default: Date.now }
});
//Here Assign For The Mkae For The Globle Varible For The acess Data
let Payment_Details = mongoose.model('Payment_Details', Payment_Schema);
module.exports.Payment_Details = Payment_Details;

//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

module.exports.crud = function(DataArray, crudStatus, where, callback) {
	/*
      @ Globle Make For The Crud Operation
      @ Globle Make For Any where Accessble Here...
      @ Globle Make Data Must Be Partision
      # crudStatus =0 means Get All Data Here..
      # crudStatus =1 means Get Where Data
      # crudStatus =2 means Insert Data...
      # crudStatus =3 means update Data...
      # crudStatus =4 means delete Data...
    */
	if (crudStatus == 0) {
		Payment_Details.find({}, function(ErrorResponse, ResponseArray) {
			if (ErrorResponse) {
				callback(null, 500);
			} else {
				callback(ResponseArray, 200);
			}
		});
	} else if (crudStatus == 1) {
		// End 0 Status Here ...
		// For The Data Get Where Condtion Here...
		Payment_Details.find(where, function(ErrorResponse, ResponseArray) {
			if (ErrorResponse) {
				//console.log(ErrorResponse);
				callback(null, 500);
			} else {
				////console.log(ResponseArray);
				callback(ResponseArray, 200);
			}
		});
	} else if (crudStatus == 2) {
		// End The Status crud Operation here ....1
		// For The Data Get Where Condtion Here...
		Payment_Details.collection.insert(DataArray, function(ErrorResponse, ResponseArray) {
			if (ErrorResponse) {
				callback(null, 500);
			} else {
				callback(ResponseArray, 200);
			}
		});
	} else if (crudStatus == 3) {
		//End The Status Crud Code Here .....3
		// For The Data Get Where Condtion Here...
		//console.log('Payment_DetailssSchema ', where, DataArray);
		Payment_Details.update(where, { $set: DataArray }, function(ErrorResponse, ResponseArray) {
			if (ErrorResponse) {
				callback(null, 500);
			} else {
				callback(ResponseArray, 200);
			}
		});
	} else if (crudStatus == 4) {
		// End Update Data Here......
		//End The Status Crud Code Here .....3
		// For The Data Get Where Condtion Here...
		Payment_Details.remove(where, function(ErrorResponse, ResponseArray) {
			if (ErrorResponse) {
				callback(null, 500);
			} else {
				callback(ResponseArray, 200);
			}
		});
	} else if (crudStatus == 5) {
		// End Update Data Here......
		//End The Status Crud Code Here .....3
		// For The Data Get Where Condtion Here...
		Payment_Details.find(where)
			.populate({ path: 'order_id', populate: { path: 'kitchen_id', populate: { path: 'category_id' } } })
			.populate({ path: 'user_id'})
			.exec(function(ErrorResponse, ResponseArray) {
				console.log(ErrorResponse, ResponseArray);
				if (ErrorResponse != null) {
					callback(null, 500);
				} else {
					callback(ResponseArray, 200);
				}
			});
	} else if (crudStatus == 6) {
		// End Update Data Here......
		//End The Status Crud Code Here .....3
		// For The Data Get Where Condtion Here...
		Payment_Details.find(where)
			.populate({ path: 'item_id', populate: { path: 'category_id' } })
			.exec(function(ErrorResponse, ResponseArray) {
				// console.log(ErrorResponse, ResponseArray);
				if (ErrorResponse != null) {
					callback(null, 500);
				} else {
					callback(ResponseArray, 200);
				}
			});
	} else {
		// End Delete Data Here......
		callback(true);
	}
};

//==================================================================================================

module.exports.user_in = function(idsArray, callback) {
	Payment_Details.find({ _id: { $in: idsArray } }, (ErrorMongo, ResponseMongo) => {
		if (ErrorMongo) {
			callback(ErrorMongo);
		} else {
			callback(ResponseMongo);
		}
	});
};

//==================================================================================*************============================================
