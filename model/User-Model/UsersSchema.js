const mongoose = require('mongoose');
const Schema = mongoose.Schema,
	ObjectId = Schema.ObjectId;

const UsersSchema = new Schema({
	kitchen_name: { type: 'String', default: '' },
	address: { type: 'String', default: '' },
	full_name: { type: 'String', default: '' },
	phone_no: { types: 'String', default: '' },
	pincode: { types: 'String', default: '' },
	email: { type: 'String', default: '' },
	logo: { type: 'String', default: '' },
	user_type: { type: 'String', default: '' },
	enabled: { type: 'Boolean', default: true },
	password: { type: 'String', default: '' },
	auth_token: { type: 'String', default: '' },
	country_code: { type: 'String', default: '' },
	min_order_amount: { type: 'Number', default: 0 },
	kitchen_title: { type: 'String', default: '' },
	profile_pic: { type: 'String', default: "" },
	country: { type: 'String', default: "" },
	stripe_token: { type: 'String', default: "" },
	stripe_keys: { type: 'Object', default: {} },
	heating: { type: 'String', default: "" },
	delivery: { type: 'String', default: "" },
	storage: { type: 'String', default: "" },
	forgot_code: { type: 'String', default: "" },
	food_type: { type: 'String', default: "fresh"}
});
//Here Assign For The Mkae For The Globle Varible For The acess Data
let User = mongoose.model('users', UsersSchema);
module.exports.User = User;

//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

module.exports.crud = function(DataArray, crudStatus, where, callback) {
	/*
      @ Globle Make For The Crud Operation
      @ Globle Make For Any where Accessble Here...
      @ Globle Make Data Must Be Partision
      # crudStatus =0 means Get All Data Here..
      # crudStatus =1 means Get Where Data
      # crudStatus =2 means Insert Data...
      # crudStatus =3 means update Data...
      # crudStatus =4 means delete Data...
    */
	if (crudStatus == 0) {
		User.find({}, function(ErrorResponse, ResponseArray) {
			if (ErrorResponse) {
				callback(null, 500);
			} else {
				callback(ResponseArray, 200);
			}
		});
	} else if (crudStatus == 1) {
		// End 0 Status Here ...
		// For The Data Get Where Condtion Here...
		User.find(where, function(ErrorResponse, ResponseArray) {
			if (ErrorResponse) {
				//console.log(ErrorResponse);
				callback(null, 500);
			} else {
				////console.log(ResponseArray);
				callback(ResponseArray, 200);
			}
		});
	} else if (crudStatus == 2) {
		// End The Status crud Operation here ....1
		// For The Data Get Where Condtion Here...
		User.collection.insert(DataArray, function(ErrorResponse, ResponseArray) {
			if (ErrorResponse) {
				callback(null, 500);
			} else {
				callback(ResponseArray, 200);
			}
		});
	} else if (crudStatus == 3) {
		//End The Status Crud Code Here .....3
		// For The Data Get Where Condtion Here...
		//console.log('UsersSchema ', where, DataArray);
		User.update(where, { $set: DataArray }, function(ErrorResponse, ResponseArray) {
			if (ErrorResponse) {
				callback(null, 500);
			} else {
				callback(ResponseArray, 200);
			}
		});
	} else if (crudStatus == 4) {
		// End Update Data Here......
		//End The Status Crud Code Here .....3
		// For The Data Get Where Condtion Here...
		User.remove(where, function(ErrorResponse, ResponseArray) {
			if (ErrorResponse) {
				callback(null, 500);
			} else {
				callback(ResponseArray, 200);
			}
		});
	} else {
		// End Delete Data Here......
		callback(true);
	}
};

//==================================================================================================

module.exports.user_in = function(idsArray, callback) {
	User.find({ _id: { $in: idsArray } }, (ErrorMongo, ResponseMongo) => {
		if (ErrorMongo) {
			callback(ErrorMongo);
		} else {
			callback(ResponseMongo);
		}
	});
};
