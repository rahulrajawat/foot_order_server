const mongoose = require('mongoose');
const Schema = mongoose.Schema,
	ObjectId = Schema.ObjectId;

const EndUsersSchema = new Schema({
	address: { type: 'String', default: '' },
	full_name: { type: 'String', default: '' },
	contact: { type: 'String', default: '' },
	postal_code: { type: 'String', default: '' },
	email: { type: 'String', default: '' },
	profile_pic: { type: 'String', default: '' },
	user_type: { type: 'String', default: '' },
	country_code: { type: 'String', default: '' },
	password: { type: 'String', default: '' },
	auth_token: { type: 'String', default: '' },
	stripe_token: { type: 'String', default: '' }
});
//Here Assign For The Mkae For The Globle Varible For The acess Data
let EndUser = mongoose.model('end_users', EndUsersSchema);
module.exports.EndUser = EndUser;

//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

module.exports.crud = function(DataArray, crudStatus, where, callback) {
	/*
      @ Globle Make For The Crud Operation
      @ Globle Make For Any where Accessble Here...
      @ Globle Make Data Must Be Partision
      # crudStatus =0 means Get All Data Here..
      # crudStatus =1 means Get Where Data
      # crudStatus =2 means Insert Data...
      # crudStatus =3 means update Data...
      # crudStatus =4 means delete Data...
    */
	if (crudStatus == 0) {
		EndUser.find({}, function(ErrorResponse, ResponseArray) {
			if (ErrorResponse) {
				callback(null, 500);
			} else {
				callback(ResponseArray, 200);
			}
		});
	} else if (crudStatus == 1) {
		// End 0 Status Here ...
		// For The Data Get Where Condtion Here...
		EndUser.find(where, function(ErrorResponse, ResponseArray) {
			if (ErrorResponse) {
				//console.log(ErrorResponse);
				callback(null, 500);
			} else {
				////console.log(ResponseArray);
				callback(ResponseArray, 200);
			}
		});
	} else if (crudStatus == 2) {
		// End The Status crud Operation here ....1
		// For The Data Get Where Condtion Here...
		EndUser.collection.insert(DataArray, function(ErrorResponse, ResponseArray) {
			if (ErrorResponse) {
				callback(null, 500);
			} else {
				callback(ResponseArray, 200);
			}
		});
	} else if (crudStatus == 3) {
		//End The Status Crud Code Here .....3
		// For The Data Get Where Condtion Here...
		//console.log('EndUsersSchema ', where, DataArray);
		EndUser.update(where, { $set: DataArray }, function(ErrorResponse, ResponseArray) {
			if (ErrorResponse) {
				callback(null, 500);
			} else {
				callback(ResponseArray, 200);
			}
		});
	} else if (crudStatus == 4) {
		// End Update Data Here......
		//End The Status Crud Code Here .....3
		// For The Data Get Where Condtion Here...
		EndUser.remove(where, function(ErrorResponse, ResponseArray) {
			if (ErrorResponse) {
				callback(null, 500);
			} else {
				callback(ResponseArray, 200);
			}
		});
	} else {
		// End Delete Data Here......
		callback(true);
	}
};

//==================================================================================================

module.exports.user_in = function(idsArray, callback) {
	EndUser.find({ _id: { $in: idsArray } }, (ErrorMongo, ResponseMongo) => {
		if (ErrorMongo) {
			callback(ErrorMongo);
		} else {
			callback(ResponseMongo);
		}
	});
};
