var include = require('../../lib/include')
var stripe = require('stripe')('sk_test_1WBiG7dCKa6IerKlOkFlborX')

//===================================================================================================
module.exports.openAnAccount = function (ResponseData, base_url, callback) {
	// Custom Insert Here Data Here...
	password = '';
	if (ResponseData.auth_type === 0) {
		password = '';
	} else {
		var password = include.auth.encryptionPassword(ResponseData.password);
	}

	var ResponseArray = {};
	var MainResponseArray = [];
	ResponseArray = ResponseData;
	ResponseArray.password = password;
	//ResponseData.country_code
	var phoneNoWithCountryCode = ResponseData.country_code + '' + ResponseData.phone_no;
	// Here Data Include Here.. Code By
	//@where condtion must be worked here...
	var where = {
		$or: [{
			phone_no: ResponseData.phone_no,
			email: ResponseData.email
		}]
	};

	var schema = '';
	if (ResponseData.user) {
		schema = 'EndUserSchema';
	} else {
		schema = 'UserSchema';
		ResponseData.enabled = true;
	}

	include[schema].crud(ResponseArray, 1, where, (ResponseResult) => {
		//callback(null,ResponseResult,200);
		// if (ResponseResult.length <= 0) {
		if (true) {

			include[schema].crud(ResponseArray, 2, null, (ResponseResult) => {
				if (!ResponseData.user) {
					console.log("visa", ResponseResult)
					include.stripe_model.subscribeUserStripe(ResponseData, base_url, (data) => {
						// console.log(data);
						include.UserSchema.crud({
							stripe_token: data.id,
							stripe_keys: data.keys
						}, 3, {
							_id: ResponseResult.insertedIds[0]
						}, (data, status) => {
							console.log(data, status)

						});
					});
					callback(null, ResponseResult, 200)

				} else {
					include.utils.sendEmail(ResponseData.email, "Welcome to Titan's secret", "welcome", ResponseData.full_name)
						.then((res) => callback(null, ResponseResult, 200))
						.catch(err => callback(err, null, 204))
				}
			});
			console.log(ResponseResult);
		} else {
			// @If User Already Exists.Thewn This Condtion
			callback(null, ResponseResult, 204);
		}
	});
};

//==========================================================================================================
module.exports.updateAnAccount = function (ResponseData, base_url, callback) {
	//Check Here Arrang eto Code For the Json Object.......
	var params = [
		'gender',
		'date_of_birth',
		'country',
		'language',
		'shouldSpeakEnglish',
		'user_lat',
		'user_long',
		'device_token'
	];
	// Here Check User Empty Or Not
	var ResponseArrayObj = include.common_helper.ValidArrayArange(params, ResponseData);
	// Where Defien By GLoble User
	//console.log('ResponseArrayObj', ResponseArrayObj);
	//console.log('updateAnAccount ', ResponseData.user_id);
	var where = {
		_id: ResponseData.user_id
	};
	include.UserSchema.crud(ResponseArrayObj, 3, where, (ResponseResult, StatusCode) => {
		// Here Response Check Send Response Code....
		if (StatusCode == 200) {
			include.UserSchema.crud(ResponseArrayObj, 1, where, (GetResponseResult, StatusCode) => {
				var GetUserData = include.common_helper.GetUserData(GetResponseResult, base_url);
				callback(
					null, {
						user: GetUserData
					},
					200
				);
			});
			//Here Response Get Similer Which User Want to a specifi data.....
		} else if (StatusCode == 500) {
			//Node Js Developer.....
			//IF Check Data Must Be Required..
			callback(null, null, 500);
		}
	});
};

//==========================================================================================================
module.exports.VerifyOTP = function (ResponseData, callback) {
	//, otp : ResponseData.otp_code

	//console.log("VerifyOTP ", ResponseData);
	var mongoose = require('mongoose');
	var id = ResponseData.user_id; //mongoose.Types.ObjectId(ResponseData.user_id);
	var where = {
		_id: id
	};
	//console.log(where);
	include.UserSchema.crud(ResponseData, 1, where, (ResponseResult, StatusCode) => {
		// Here Response Check Send Response Code....

		//console.log(ResponseResult);
		if (StatusCode == 200) {
			// Verify Otp Here..verifyUser
			if (ResponseResult.length > 0) {
				var nexmo_id = ResponseResult[0].nexmo_id;
				//console.log(nexmo_id);
				include.nexmo.verifyUser(nexmo_id, ResponseData.otp_code, function (NexmoResponse) {
					if (NexmoResponse.status == 0) {
						var updateArray = {
							is_verify: 1
						};
						var whereNexmo = {
							nexmo_id: nexmo_id
						};
						//console.log(whereNexmo);
						include.UserSchema.crud(updateArray, 3, where, (ResponseResultUpdated, StatusCodeUpdate) => {
							// Here Included Data Which Have Required...
							//if(StatusCodeUpdate ==200){
							callback(null, [], 200);
							// }
						});
						//callback(null,[],200);
					} else {
						callback(null, NexmoResponse.error_text, 500);
					}
				});
			} else {}
		}
		//Node Js Developer.....
	});
};

//==========================================================================================================
module.exports.ResendOTP = function (ResponseData, callback) {
	////console.log(ResponseData);

	var where = {
		_id: ResponseData.user_id
	};
	include.UserSchema.crud(ResponseData, 1, where, (ResponseResult, StatusCode) => {
		// //console.log(ResponseResult);
		// Here Response Check Send Response Code....
		// //console.log(ResponseResult);
		//console.log(ResponseResult);
		if (StatusCode == 200) {
			// Verify Otp Here..
			if (ResponseResult.length > 0) {
				//console.log(ResponseResult);
				include.nexmo.sendOtpByUsers('91' + ResponseResult[0].phone_no, function (OtpResponse) {
					var nexmo_request = JSON.parse(OtpResponse);
					var nexmo_req_id = nexmo_request.request_id;
					// //console.log(nexmo_req_id);
					var update_nexmo_id = {
						nexmo_id: nexmo_req_id,
						is_verify: 0
					};
					include.UserSchema.crud(update_nexmo_id, 3, where, (ResponseResultUpdated, StatusCodeUpdate) => {
						// Here Included Data Which Have Required...
						if (StatusCodeUpdate == 200) {
							callback(null, [], 200);
						}
					});
				});
			} else {
				callback(null, ResponseResult, 500);
			}
		}
	});
};
//========================================Twilio===========================================

module.exports.AuthenticateUser = function (ResponseData, base_url, callback) {
	console.log("login api ", ResponseData);
	var password = include.auth.encryptionPassword(ResponseData.password);
	var where = {
		email: ResponseData.email
	};
	var schema = 'UserSchema';
	if (ResponseData.user) {
		schema = 'EndUserSchema';
	}

	include[schema].crud(null, 1, where, (ResponseResult, StatusCode) => {
		// Here Response Check Send Response Code....
		//@ Here Check Only Username Is Valid Or Not
		//@ If User Name is Invalid So This Condtion Is Callback here...
		if (StatusCode == 200) {
			// If Username is Valid Or True this condtion forword ahead  or
			// Back Again Back Side ....
			if (ResponseResult.length > 0) {
				var WhereUsernameAndPassword = {};

				WhereUsernameAndPassword = {
					email: ResponseData.email,
					password: password,
				};
				//console.log('test', WhereUsernameAndPassword);

				// Here Complite If Username And Passowrd Is valid Or not is here check...
				include[schema].crud(null, 1, WhereUsernameAndPassword, (ResponseResultLogin, StatusCodeLogin) => {
					console.log(ResponseResultLogin, StatusCodeLogin);
					// access_token: include.auth.access_token(ResponseResultLogin[0].user_id),
					//console.log('some response', ResponseResultLogin, StatusCodeLogin);
					// Here Included Data Which Have Required...
					// //console.log(ResponseResultLogin, StatusCodeLogin);
					if (ResponseResultLogin.length > 0) {

						if (!ResponseData.user && !ResponseResultLogin[0].enabled) {
							callback(
								[{
									error: true
								}],
								null,
								204,
								"Your account has been disabled! Please Contact Admin."
							);
						} else {

							var auth_token = include.auth.access_token(ResponseResultLogin[0]._id);
							var updateAccessTokenArray = {
								auth_token: auth_token
							};

							ResponseResultLogin[0].auth_token = auth_token;
							include[
								schema
							].crud(
								updateAccessTokenArray,
								3,
								WhereUsernameAndPassword,
								(ErrorUpdateAccessToken, ResponseUpdateArray) => {
									include[schema].crud(null, 1, WhereUsernameAndPassword, (FResponse, Error) => {
										// Here 200 Code success Codition ....
										callback(null, FResponse, 200, '');
									});
								}
							);
						}

					} else {
						callback(
							[{
								error: true
							}],
							null,
							204,
							include.message.message.login_invalid_password
						);
					}
				});
			} else {
				// This Function This Condtion Runnble When UserName Is Invalid
				callback(
					[{
						error: true
					}],
					null,
					204,
					include.message.message.login_invalid_username
				);

				// callback(null, ResponseResult, 205, include.message.message.UserNameIsInvalid);
			}
		}
	});
};

module.exports.changePassword = function (ResponseData, base_url, callback) {
	var password = include.auth.encryptionPassword(ResponseData.password);
	var where = {
		email: ResponseData.email
		// password: password
	};

	var schema = 'UserSchema';
	if (ResponseData.usertype == 'enduser') {
		schema = 'EndUserSchema';
	}

	include[schema].crud(null, 1, where, (ResponseResult, StatusCode) => {
		// Here Response Check Send Response Code....
		//@ Here Check Only Username Is Valid Or Not
		//@ If User Name is Invalid So This Condtion Is Callback here...
		if (StatusCode == 200) {
			// If Username is Valid Or True this condtion forword ahead  or
			// Back Again Back Side ....
			if (ResponseResult.length > 0) {
				// var auth_token = include.auth.access_token(ResponseResultLogin[0]._id);
				var updatePassword = {
					password: password
				};

				// ResponseResultLogin[0].auth_token = auth_token;
				include[schema].crud(updatePassword, 3, where, (ErrorUpdateAccessToken, ResponseUpdateArray) => {
					include[schema].crud(null, 1, where, (FResponse, Error) => {
						// Here 200 Code success Codition ....
						callback(null, FResponse, 200, '');
					});
				});
			} else {
				// This Function This Condtion Runnble When UserName Is Invalid
				callback(
					[{
						error: true
					}],
					null,
					204,
					include.message.message.login_invalid_username
				);

				// callback(null, ResponseResult, 205, include.message.message.UserNameIsInvalid);
			}
		}
	});
};

module.exports.listKitchens = function (ResponseData, callback) {
	var where = {
		user_type: 'kitchen'
	};
	include.UserSchema.crud(ResponseData, 1, where, (ResponseResult, StatusCode) => {
		callback(null, ResponseResult, StatusCode);
	});
};

//========================================Twilio===========================================

module.exports.forgotPassword = function (ResponseData, callback) {
	var where = {
		email: ResponseData.email
	};
	var schema = ''
	if (ResponseData.usertype == "user") {
		schema = 'EndUserSchema';
	} else {
		schema = 'UserSchema';
		// ResponseData.enabled = true;
	}
	include[schema].crud(ResponseData, 1, where, (ResponseResult, StatusCode) => {
		// Verify Otp Here..
		if (StatusCode == 200) {
			var updateArray = {
				forgot_code: Math.floor(Math.random() * Math.floor(6))
			};
			include.UserSchema.crud(updateArray, 3, where, (ResponseResultUpdated, StatusCodeUpdate) => {
				// Here Included Data Which Have Required...
				if (StatusCodeUpdate == 200) {
					include.utils.sendEmail(ResponseData.email, "Reset your Titan's Secret password", "resetpassword", ResponseResult[0].full_name,updateArray).then((succ) => {
						callback(null, succ, 200);
					}).catch((succ) => {
						callback(succ, null, 500);
					})
				}
			});
			
		} else {
			callback([{}], [], 500);
		}

	});
}
//========================================Twilio===========================================

module.exports.getNearUserListByUserId = function (ResponseData, callback) {
	var where = {
		_id: {
			$ne: ResponseData.user_id
		}
	};
	//console.log(where);
	include.UserSchema.crud(ResponseData, 1, where, (ResponseResult, StatusCode) => {
		// Here Response Check Send Response Code....
		//console.log(ResponseResult);
		if (StatusCode == 200) {
			//=====================================================

			//in future use for flaguser check

			//=====================================================
			if (ResponseResult.length > 0) {
				var i = 0;
				var MainResponseArray = [];
				ResponseResult.forEach((row) => {
					MainResponseArray1 = {
						user_id: row._id,
						username: row.username,
						profile_pic: 'http://qrit.hiteshi.com:1212/profile_pic/default.jpg'
					};
					MainResponseArray[i] = MainResponseArray1;
					i++;
				});

				callback(
					null, {
						users: MainResponseArray
					},
					200
				);
			} else {
				callback(null, [], 500);
			}
		}
	});
};

//==========================================================================================================

module.exports.LogoutUserByUserId = function (ResponseData, callback) {
	var where = {
		_id: ResponseData.user_id
	};
	include.UserSchema.crud(ResponseData, 1, where, (ResponseResult, StatusCode) => {
		// Here Response Check Send Response Code....
		//console.log(ResponseResult);

		if (StatusCode == 200) {
			if (ResponseResult.length > 0) {
				var updateArray = {
					login_status: 1
				};
				include.UserSchema.crud(updateArray, 3, where, (ResponseResultUpdated, StatusCodeUpdate) => {
					// Here Included Data Which Have Required...
					if (StatusCodeUpdate == 200) {
						callback(null, updateArray, 200);
					}
				});
			} else {
				callback(null, ResponseResult, 500);
			}
		} else {
			callback(null, ResponseResult, 500);
		}
	});
};
//============================================================ --------- User Logout Model End ---------- ========================================================

//============================================================ --------- Get User Model End ---------- ========================================================
module.exports.locationUpdateByUserId = function (ResponseData, base_url, callback) {
	//Check Here Arrang eto Code For the Json Object.......
	var params = ['address', 'city', 'country', 'range'];
	// Here Check User Empty Or Not
	var ResponseArrayObj = include.common_helper.ValidArrayArange(params, ResponseData);
	// Where Defien By GLoble User
	var where = {
		_id: ResponseData.user_id
	};
	include.UserSchema.crud(ResponseArrayObj, 3, where, (ResponseResult, StatusCode) => {
		// Here Response Check Send Response Code....
		if (StatusCode == 200) {
			include.UserSchema.crud(ResponseArrayObj, 1, where, (GetResponseResult, StatusCode) => {
				var GetUserData = include.common_helper.GetUserData(GetResponseResult, base_url);
				callback(
					null, {
						user: GetUserData
					},
					200
				);
			});
			//Here Response Get Similer Which User Want to a specifi data.....
		} else if (StatusCode == 500) {
			//Node Js Developer.....
			//IF Check Data Must Be Required..
			callback(null, null, 500);
		}
	});
};

//============================================================ --------- Get User Model End ---------- ========================================================
// module.exports.locationUpdateByUserId =function(ResponseData,callback){
//   //Check Here Arrang eto Code For the Json Object.......
// var params=['username','city','country',"range","phone_no","currency"];
// // Here Check User Empty Or Not
// var ResponseArrayObj =include.common_helper.ValidArrayArange(params,ResponseData);
// // Where Defien By GLoble User
// var where ={_id :ResponseData.user_id};
// include.UserSchema.crud(ResponseArrayObj,3,where,(ResponseResult,StatusCode)=>{
//    // Here Response Check Send Response Code....
//    if(StatusCode ==200){
//       include.UserSchema.crud(ResponseArrayObj,1,where,(GetResponseResult,StatusCode)=>{
//          var GetUserData =include.common_helper.GetUserData(GetResponseResult,base_url);
//           callback(null,{user:GetUserData},200);
//       });
//      //Here Response Get Similer Which User Want to a specifi data.....

//    }
//    //Node Js Developer.....
//    else if(StatusCode ==500){
//      //IF Check Data Must Be Required..
//      callback(null,null,500);
//    }

// });

// }

module.exports.resetPassword = function (ResponseData, callback) {


	var email = ResponseData.email;
	var where = {
		email: email
	};
	//console.log(where);

	var schema = ''
	if (ResponseData.usertype == "user") {
		schema = 'EndUserSchema';
	} else {
		schema = 'UserSchema';
		// ResponseData.enabled = true;
	}
	include[schema].crud(ResponseData, 1, where, (ResponseResult, StatusCode) => {
		// Here Response Check Send Response Code....
		//console.log(ResponseResult);
		if (StatusCode == 200) {
			// Verify Otp Here..verifyUser
			if (ResponseResult.length > 0) {

				var new_password = include.auth.encryptionPassword(ResponseData.new_password);
				var updateArray = {
					password: new_password
				};
				include[schema].crud(updateArray, 3, where, (ResponseResultUpdated, StatusCodeUpdate) => {
					callback(null, [{
						error: false,
						message: "Updated succesfully"
					}], 200);
				});

			} else {
				callback([{
					error: true,
					message: "something went wrong"
				}], null, 500);
			}
		}
		//Node Js Developer.....
	});
};

//============================================================================================
module.exports.profileUpdateByUserId = function (ResponseData, reqData, base_url, callback) {
	//Check Here Arrang eto Code For the Json Object.......
	var username = ResponseData.username;
	var password = ResponseData.password;
	var phone_no = ResponseData.phone_no;
	var country_code = ResponseData.country_code;
	var currency = ResponseData.currency;
	var pincode = ResponseData.pincode;
	// "kitchen_name":"new Shreemaya Celebrations",
	// "address":"Madhumilan Square",
	// "pincode":"452002",
	// "phone_no":"7896541320",
	// "email":"abbcd@gmail.com",
	// "logo":"somelogoimage.jpg",
	// "country_code":"91",
	// "password":"1234567",
	// "enabled":true
	var UpdateResponse = {};
	if (username !== '') {
		UpdateResponse.username = username;
	}

	//==========================================================
	/*if(password!=""){
        var password1 =include.auth.encryptionPassword(password);
      UpdateResponse.password =password1;
    }*/

	//==========================================================
	if (phone_no != '') {
		UpdateResponse.phone_no = phone_no;
	}

	if (pincode != '') {
		UpdateResponse.pincode = pincode;
	}

	//==========================================================

	if (currency != '') {
		UpdateResponse.currency = currency;
	}

	if (country_code != '') {
		UpdateResponse.country_code = country_code;
	}

	if (country_code != '') {
		UpdateResponse.food_type = ResponseData.food_type;
	}

	//==========================================================

	var where = {
		_id: ResponseData.user_id
	};
	//console.log("======================================Where============");
	//console.log(where);
	//console.log("======================================Where============");
	include.UserSchema.crud(UpdateResponse, 3, where, (ResponseResult, StatusCode) => {
		// Here Response Check Send Response Code....
		//console.log(ResponseResult);
		if (StatusCode == 200) {
			include.UserSchema.crud(null, 0, where, (GetResponseResult, StatusCode) => {
				var GetUserData = include.common_helper.GetUserData(GetResponseResult, base_url);
				callback(
					null, {
						user: GetUserData
					},
					200
				);
			});
			//Here Response Get Similer Which User Want to a specifi data.....
		} else if (StatusCode == 500) {
			//Node Js Developer.....
			//IF Check Data Must Be Required..
			callback(null, null, 500);
		}
	});
};

//==================================================================================================

module.exports.updateNotificationSettingsByUserId = function (ResponseData, base_url, callback) {
	//Check Here Arrang eto Code For the Json Object.......

	// Where Defien By GLoble User
	var where = {
		_id: ResponseData.user_id
	};
	include.UserSchema.crud({
			push_notification_enable: ResponseData.shouldEnable
		},
		3,
		where,
		(ResponseResult, StatusCode) => {
			// Here Response Check Send Response Code....
			if (StatusCode == 200) {
				include.UserSchema.crud(null, 1, where, (GetResponseResult, StatusCode) => {
					var GetUserData = include.common_helper.GetUserData(GetResponseResult, base_url);
					callback(
						null, {
							user: GetUserData
						},
						200
					);
				});
				//Here Response Get Similer Which User Want to a specifi data.....
			} else if (StatusCode == 500) {
				//Node Js Developer.....
				//IF Check Data Must Be Required..
				callback(null, null, 500);
			}
		}
	);
};

//=================================================================================================

module.exports.getAllNotifications = function (user_id, access_token, callback) {
	//console.log(user_id);
	var where = {
		reciever_id: user_id
		/* access_token: access_token*/
	};
	include.NotificationSchema.crud(null, 1, where, (ResponseResult, StatusCode) => {
		//console.log("hellso", where);
		//console.log(ResponseResult);
		callback(ResponseResult, StatusCode);
	});
};
//=================================================================================================

module.exports.deleteNotification = function (notification_id, callback) {
	var where = {
		_id: notification_id
	};
	include.NotificationSchema.crud(null, 4, where, (ResponseResult, StatusCode) => {
		callback(ResponseResult, StatusCode);
		//console.log(ResponseResult);
		/*if (ResponseResult.length > 0) {

        } else {
            callback(false);
        }*/
	});
};

//to do getKitchenDetailbyId api
module.exports.getKitchenDetailbyId = function (ResponseData, callback) {
	var kitchen_id = ResponseData.kitchen_id;
	var category_id = ResponseData.category_id;
	var where = {
		kitchen_id: kitchen_id
	};

	// //console.log(where);
	include.kitchenFeature_schema.crud(null, 1, where, (ResponseResult, StatusCode) => {
		// Here Response Check Send Response Code....
		if (ResponseResult.length > 0) {
			var kitchenFeature = {};

			ResponseResult.forEach(function (element) {
				//console.log(element.kitchen_id);
				kitchenFeature[element.kitchen_id] = element;
			}, this);
			// //console.log(JSON.stringify(kitchenFeature));
			if (category_id) {
				where = {
					$and: [{
						kitchen_id: kitchen_id
					}, {
						category_id: {
							$in: category_id
						}
					}]
				};
			} else {
				where = {
					kitchen_id: kitchen_id
				}
			}
			include.KitchenSchema.crud(null, 6, where, (kitchenResult, StatusCode) => {
				// //console.log(kitchenResult, StatusCode);
				var finalres = [];
				// //console.log("res",ResponseResult)
				if (kitchenResult.length > 0) {
					var menuItem = {};

					for (var index = 0; index < kitchenResult.length; index++) {
						var element = kitchenResult[index];
						// rowData.kitchenFeature = kitchenFeature[element.kitchen_id._id];
						if (menuItem[element.kitchen_id] == undefined) {
							menuItem[element.kitchen_id] = [];
						}
						menuItem[element.kitchen_id].push(kitchenResult[index]);
					}

					// //console.log('menuItem', JSON.stringify(kitchenResult));

					include.UserSchema.crud(null, 1, {
						_id: kitchen_id
					}, (userResult, StatusCode) => {
						if (userResult.length > 0) {
							// //console.log(userResult.length,userResult);
							for (let r = 0; r < userResult.length; r++) {
								var userObj = userResult[r];
								var menuData = menuItem[userObj._id];
								if (menuData != null && menuData != undefined) {
									finalres.push({
										kitchenDetails: userObj,
										menuDetails: menuData,
										kitchenFeature: ResponseResult
									});
								}
							}

							callback(null, finalres, 200);
						} else {
							callback([{
								error: true,
								message: 'No data found for your search'
							}], null, 500);
						}
					});
				} else {
					callback([{
						error: true,
						message: 'No data found for your search'
					}], null, 500);
				}
			});
		} else {
			callback([{
				error: true,
				message: 'No data found for your search'
			}], null, 500);
		}
	});
};