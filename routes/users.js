var express = require('express');
var router = express.Router();
let include = require('../lib/include');
var multer = require('multer');

router.post(include.constant.methods.AddAccount, function (req, res, next) {
	var paramsInclude = [
		'kitchen_name',
		'address',
		'full_name',
		'phone_no',
		'email',
		'logo',
		'user_type',
		'country_code',
		'password',
		'country'
	];
	let base_url = req.protocol + '://' + req.get('host');
	//@ IF ........requiredParams
	if (include.required.requiredParams(paramsInclude, req.body) == true) {
		req.body.user = false;

		include.UserModel.openAnAccount(
			req.body,
			req.connection.remoteAddress,
			(ErrorResponse, SuccessResponse, status) => {
				if (ErrorResponse) {
					// @ Error Response code Here......
					res.json(include.HTTP_RESPONSE.errorsWithMessage(ErrorResponse));
				} else if (status == 200) {
					// @ Response Set Defiend.......
					res.json(
						include.HTTP_RESPONSE.successWithResponse(
							include.message.message.signup_success,
							SuccessResponse
						)
					);
				} else if (status == 204) {
					//User Already Exists. Then This Condtion Must Be Required.
					res.json(
						include.HTTP_RESPONSE.successWithResponse(include.message.message.signup_account_exists, [])
					);
				}
			}
		);
		//@ End Else Here........
	} else {
		// Validation Must Be Required.
		res.json(include.required.requiredParams(paramsInclude, req.body));
	}
});

router.post(include.constant.methods.AuthenticateUser, function (req, res, next) {
	var paramsInclude = ['email', 'password'];
	let base_url = req.protocol + '://' + req.get('host');
	//@ IF ........requiredParams
	if (include.required.requiredParams(paramsInclude, req.body) == true) {
		include.UserModel.AuthenticateUser(req.body, base_url, (ErrorResponse, SuccessResponse, status, msg) => {
			if (ErrorResponse) {
				res.json(include.HTTP_RESPONSE.errorWithResponse(msg, ErrorResponse));
			} else if (status == 200) {
				// @ Response Set Defiend.......
				res.json(
					include.HTTP_RESPONSE.successWithResponse(include.message.message.login_success, SuccessResponse)
				);
			} else {
				//User Already Exists. Then This Condtion Must Be Required.
				res.json(include.HTTP_RESPONSE.successWithResponse(msg, []));
			}
		});
		//@ End Else Here........
	} else {
		// Validation Must Be Required.
		res.json(include.required.requiredParams(paramsInclude, req.body));
	}
});

router.post(include.constant.methods.loginUsers, function (req, res, next) {
	var paramsInclude = ['email', 'password'];
	let base_url = req.protocol + '://' + req.get('host');
	//@ IF ........requiredParams
	if (include.required.requiredParams(paramsInclude, req.body) == true) {
		req.body.user = true;

		include.UserModel.AuthenticateUser(req.body, base_url, (ErrorResponse, SuccessResponse, status, msg) => {
			console.log(ErrorResponse, SuccessResponse, status, msg);
			if (ErrorResponse) {
				res.json(include.HTTP_RESPONSE.errorWithResponse(msg, ErrorResponse));
			} else if (status == 200) {
				// @ Response Set Defiend.......
				res.json(
					include.HTTP_RESPONSE.successWithResponse(include.message.message.login_success, SuccessResponse)
				);
			} else {
				//User Already Exists. Then This Condtion Must Be Required.
				res.json(include.HTTP_RESPONSE.successWithResponse(msg, []));
			}
		});
		//@ End Else Here........
	} else {
		// Validation Must Be Required.
		res.json(include.required.requiredParams(paramsInclude, req.body));
	}
});

router.post(include.constant.methods.changePassword, function (req, res, next) {
	var paramsInclude = ['email', 'password', 'usertype'];

	let base_url = req.protocol + '://' + req.get('host');
	//@ IF ........requiredParams
	if (include.required.requiredParams(paramsInclude, req.body) == true) {
		if (req.body.email == '') {
			res.json(
				include.HTTP_RESPONSE.errorWithResponse('Please provide email in order to update your password', [])
			);
			return;
		}
		if (req.body.password == '') {
			res.json(include.HTTP_RESPONSE.errorWithResponse('Password can not be empty', []));
			return;
		}
		include.UserModel.changePassword(req.body, base_url, (ErrorResponse, SuccessResponse, status, msg) => {
			if (ErrorResponse) {
				res.json(include.HTTP_RESPONSE.errorWithResponse(msg, ErrorResponse));
			} else if (status == 200) {
				// @ Response Set Defiend.......
				res.json(include.HTTP_RESPONSE.successWithResponse('Password changed successfully', SuccessResponse));
			} else {
				//User Already Exists. Then This Condtion Must Be Required.
				res.json(include.HTTP_RESPONSE.errorWithResponse('Something went wrong please try again', []));
			}
		});
		//@ End Else Here........
	} else {
		// Validation Must Be Required.
		res.json(include.required.requiredParams(paramsInclude, req.body));
	}
});

router.post(include.constant.methods.listKitchenMasters, function (req, res, next) {
	// var paramsInclude = ['email', 'password'];
	let base_url = req.protocol + '://' + req.get('host');
	include.UserModel.listKitchens(req.body, (ErrorResponse, SuccessResponse, status) => {
		if (ErrorResponse) {
			res.json(include.HTTP_RESPONSE.errorsWithMessage(ErrorResponse));
		} else if (status == 200) {
			// @ Response Set Defiend.......
			res.json(include.HTTP_RESPONSE.successWithResponse('listing', SuccessResponse));
		} else {
			//User Already Exists. Then This Condtion Must Be Required.
			res.json(include.HTTP_RESPONSE.successWithResponse(msg, []));
		}
	});
});

router.post(include.constant.methods.getAccountSettings, function (req, res, next) {
	var paramsInclude = ['user_id'];

	if (include.required.requiredParams(paramsInclude, req.body) == true) {
		include.UserSchema.crud(null, 1, {
			_id: req.body.user_id
		}, (ResultArray, ResponseGet) => {
			// //console.log(KitchenResultArray, ResponseGet);
			if (ResultArray) {
				res.json(
					include.HTTP_RESPONSE.successWithResponse('listing', {
						info: ResultArray
					})
				);
			} else {
				//User Already Exists. Then This Condtion Must Be Required.
				res.json(include.HTTP_RESPONSE.errorWithResponse('No Data found', []));
			}
		});
	} else {
		// Validation Must Be Required.
		res.json(include.required.requiredParams(paramsInclude, req.body));
	}
});


var storage = multer.diskStorage({
	//multers disk storage settings
	destination: function (req, file, cb) {
		cb(null, './images/');
	},
	filename: function (req, file, cb) {
		var datetimestamp = Date.now();
		cb(
			null,
			file.fieldname +
			'-' +
			datetimestamp +
			'.' +
			file.originalname.split('.')[file.originalname.split('.').length - 1]
		);
	}
});

var upload = multer({
	//multer settings
	storage: storage
}).any( /* 'image', 5 */ );

//=================================================================================================================.

router.put(include.constant.methods.profileUpdateByUserId, function (req, res, next) {
	console.log('called 1');
	upload(req, res, function (err) {
		//console.log('update param', req.body);

		if (req.file !== undefined) {
			profile_pic = req.file.filename;
			//console.log('req.body.user_id', req.body.user_id);
			include.UserSchema.User.update({
					_id: req.body.user_id
				}, {
					$set: {
						logo: profile_pic,
						active_cxc: 1
					}
				},
				function (ErrorUpdate, ResponseUpdateSuccess) {
					//console.log(ResponseUpdateSuccess)00000;
				}
			);
		}
		var responseInsert = new Object();

		console.log("profileUpdateByUserId", req.body);

		var {
			kitchen_name,
			address,
			full_name,
			phone_no,
			pincode,
			email,
			logo,
			user_type,
			enabled,
			password,
			country_code,
			min_order_amount,
			kitchen_title,
			profile_pic,
			country,
			delivery,
			heating,
			storage,
			food_type
		} = req.body;

		if (delivery != '' && delivery != null) {
			// console.log("full_name",full_name);
			responseInsert.delivery = delivery;
		}
		if (storage != '' && storage != null) {
			// console.log("full_name",full_name);
			responseInsert.storage = storage;
		}
		if (heating != '' && heating != null) {
			// console.log("full_name",full_name);
			responseInsert.heating = heating;
		}

		if (kitchen_name != '' && kitchen_name != null) {
			// console.log("kitchen_name",kitchen_name);
			responseInsert.kitchen_name = kitchen_name;
		}
		if (address != '' && address != null) {
			// console.log("address",address);
			responseInsert.address = address;
			// console.log("responseInsert",responseInsert);

		}
		if (full_name != '' && full_name != null) {
			// console.log("full_name",full_name);
			responseInsert.full_name = full_name;
		}
		if (phone_no != '' && phone_no != null) {
			console.log("phone_no", phone_no);
			responseInsert.phone_no = phone_no;
		}
		if (pincode != '' && pincode != null) {
			// console.log("pincode",pincode);
			responseInsert.pincode = pincode;
		}
		if (email != '' && email != null) {
			// console.log("email",email);
			responseInsert.email = email;
		}
		if (logo != '' && logo != null) {
			// console.log("logo",logo);
			responseInsert.logo = logo;
		}
		if (user_type != '' && user_type != null) {
			// console.log("user_type",user_type);
			responseInsert.user_type = user_type;
		}
		if (enabled != '' && enabled != null) {
			// console.log("enabled",enabled);
			responseInsert.enabled = enabled;
		}
		if (password != '' && password != null) {
			// console.log("password",password);
			responseInsert.password = password;
		}
		if (country_code != '' && country_code != null) {
			// console.log("country_code",country_code);
			responseInsert.country_code = country_code;
		}
		if (min_order_amount != '' && min_order_amount != null) {
			// console.log("min_order_amount",min_order_amount);
			responseInsert.min_order_amount = min_order_amount;
		}
		if (kitchen_title != '' && kitchen_title != null) {
			// console.log("kitchen_title",kitchen_title);
			responseInsert.kitchen_title = kitchen_title;
		}
		if (profile_pic != '' && profile_pic != null) {
			// console.log("profile_pic",profile_pic);
			responseInsert.profile_pic = profile_pic;
		}
		if (country != '' && country != null) {
			// console.log("country",country);
			responseInsert.country = country;
		}

		if (food_type != '' && food_type != null) {
			// console.log("country",country);
			responseInsert.food_type = food_type;
		}

		console.log("responseInsert", responseInsert);
		if (Object.keys(responseInsert).length > 0) {
			include.UserSchema.User.update({
					_id: req.body.user_id
				}, {
					$set: responseInsert
				},
				function (ErrorUpdate, ResponseUpdateSuccess) {
					console.log(ErrorUpdate, ResponseUpdateSuccess);
				}
			);
		}
		res.json(include.HTTP_RESPONSE.successWithResponse('Your account update successfully.', []));
	});
});

function convertToJSON(array) {
	var arr = array[0].filter(Boolean);

	// //console.log(array[0]);
	// //console.log(array[0]);
	var first = arr.join();
	first = first.replace(' ', '_');
	var headers = first.split(',');

	var jsonData = [];
	for (var i = 1; i < array.length; i++) {
		var myRow = array[i].join();
		// //console.log(myRow);
		if (myRow == null || myRow == '') {
			// //console.log('Vishal');
			break;
		}
		var row = myRow.split(',');
		// //console.log(row.length);

		var data = {};
		for (var x = 0; x < headers.length; x++) {
			// //console.log();
			if (row[x] != undefined) {
				var element = row[x];
				data[headers[x]] = element;
			} else {
				data[headers[x]] = null;
			}
		}

		jsonData.push(data);
	}
	return jsonData;
}

var storageExcel = multer.diskStorage({
	//multers disk storage settings
	destination: function (req, file, cb) {
		// //console.log("thefile",file);
		//console.log(req.body);

		cb(null, './excels/');
	},
	filename: function (req, file, cb) {
		// //console.log(file);
		var datetimestamp = Date.now();
		cb(null, file.originalname);
	}
});

var uploadExcel = multer({
	//multer settings
	storage: storageExcel
}).single('ExcelFiles', 1);
var weeks = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
var xlsx = require('node-xlsx');
router.post(include.constant.methods.excelImport, function (req, res, next) {
	// Parse a file

	uploadExcel(req, res, function (err) {
		if (req.file == undefined || req.body.kitchen_id == undefined || req.body.kitchen_id == null) {
			res.json(
				include.HTTP_RESPONSE.errorWithResponse('Error please check your req params', [{
					error: true,
					message: 'Error please check your req params'
				}])
			);
			return;
		}
		include.kitchenFeature_schema.crud(
			null,
			4, {
				kitchen_id: req.body.kitchen_id
			},
			(RemoveResponse, RemoveStatus) => {
				if (RemoveStatus == 500) {
					res.json(
						include.HTTP_RESPONSE.errorWithResponse('Something went wrong please try again', [{
							error: true,
							message: 'Something went wrong please try again'
						}])
					);
					return;
				} else {
					//console.log(req.body.kitchen_id);
					var kitchen_id = req.body.kitchen_id;
					// //console.log(req.file);
					// var excelPath = __dirname + '/excels/cuisine.xlsx';
					var excelPath = './excels/' + req.file.filename;
					// //console.log(excelPath);
					var array = xlsx.parse(excelPath, {
						raw: true
					});
					var sheetData = convertToJSON(array[0].data);
					// //console.log(sheetData[0]);
					var finalArr = [];
					for (var p = 0; p < sheetData.length; p++) {
						var rowObj = sheetData[p];
						var dayObj = new Object();

						if (p == 0) {
							for (var w = 0; w < weeks.length; w++) {
								dayObj[weeks[w]] = null;
							}
						}
						Object.keys(rowObj).forEach(function (key, index) {
							// key: the name of the object key
							// index: the ordinal position of the key within the object
							var keyname = key.split('_');
							if (keyname.length > 2) {
								try {
									dayObj[keyname[0]][keyname[1]][keyname[2]] = rowObj[key];
								} catch (error) {
									// //console.log(keyname[0]+" != "+oldkey+" => "+count);
									if (dayObj[keyname[0]] != undefined) {
										//for d2 data
										//console.log(keyname[0]);
										dayObj[keyname[0]][keyname[1]] = {
											[keyname[2]]: rowObj[key]
										};
									} else {
										dayObj[keyname[0]] = {
											[keyname[1]]: {
												[keyname[2]]: rowObj[key]
											}
										};
									}
								}
							} else {
								var nkey = key.replace(' ', '_');
								dayObj[nkey] = rowObj[key];
							}
							// //console.log('inter res ', dayObj);
						});
						dayObj.kitchen_id = kitchen_id;
						finalArr.push(dayObj);
					}

					include.kitchenFeature_schema.crud(
						finalArr,
						2, {
							kitchen_id: req.body.kitchen_id
						},
						(ResponseResultArray, ResponseGet) => {
							res.json(
								include.HTTP_RESPONSE.successWithResponse('Success', [{
									error: false,
									message: 'Excel import successful.'
								}])
							);
						}
					);
				}

				// res.json({ data: finalArr });
			}
		);
	});
});

router.post(include.constant.methods.kitchenSchedulebyId, function (req, res, next) {
	var paramsInclude = ['kitchen_id'];
	let base_url = req.protocol + '://' + req.get('host');
	if (include.required.requiredParams(paramsInclude, req.body) == true) {
		include.UserSchema.crud(null, 1, {
			_id: req.body.kitchen_id
		}, (KitchenResultArray, ResponseGet) => {
			// //console.log(KitchenResultArray, ResponseGet);
			if (KitchenResultArray.length > 0) {
				include.kitchenFeature_schema.crud(
					null,
					1, {
						kitchen_id: req.body.kitchen_id
					},
					(ResponseResultArray, ResponseGet) => {
						// @ Response Set Defiend.......
						res.json(
							include.HTTP_RESPONSE.successWithResponse('listing', [{
								kitchen_id: KitchenResultArray[0]._id,
								country_code: KitchenResultArray[0].country_code,
								enabled: KitchenResultArray[0].enabled,
								user_type: KitchenResultArray[0].user_type,
								email: KitchenResultArray[0].email,
								full_name: KitchenResultArray[0].full_name,
								kitchen_name: KitchenResultArray[0].kitchen_name,
								address: KitchenResultArray[0].address,
								kitchenSchedule: ResponseResultArray
							}])
						);
					}
				);
			} else {
				//User Already Exists. Then This Condtion Must Be Required.
				res.json(include.HTTP_RESPONSE.errorWithResponse('No Data found', []));
			}
		});
	} else {
		// Validation Must Be Required.
		res.json(include.required.requiredParams(paramsInclude, req.body));
	}
});
router.post(include.constant.methods.updateKitchenFeatureById, function (req, res, next) {
	var paramsInclude = [
		'id',
		'Delivery_Price',
		'Sunday',
		'Monday',
		'Tuesday',
		'Wednesday',
		'Thursday',
		'Friday',
		'Sunday',
		'Postal_Code'
	];
	//console.log('Tuesday ', req.body.Tuesday);
	let base_url = req.protocol + '://' + req.get('host');
	if (include.required.requiredParams(paramsInclude, req.body) == true) {
		var responseInsert = {};
		// //console.log(req.body.Monday)
		responseInsert = {
			// Delivery_Price: req.body.Delivery_Price,
			// Sunday: req.body.Sunday,
			Monday: req.body.Monday
			// Tuesday: req.body.Tuesday,
			// Wednesday: req.body.Wednesday,
			// Thursday: req.body.Thursday,
			// Friday: req.body.Friday,
			// Saturday: req.body.Sunday,
			// Postal_Code: req.body.Postal_Code
		};
		//console.log('res', responseInsert);

		include.kitchenFeature_schema.KitchenFeature.findOne({
				_id: req.body.id
			}
			/* , {
				$set: {"Monday": req.body.Monday.D1}
			} */
			,
			function (ErrorUpdate, ResponseUpdateSuccess) {
				//console.log(ErrorUpdate, ResponseUpdateSuccess);
				if (ErrorUpdate == null) {
					// @ Response Set Defiend.......
					ResponseUpdateSuccess.update(responseInsert, function (err, Success) {
						//console.log('Success', Success, responseInsert);
						res.json(
							include.HTTP_RESPONSE.successWithResponse(
								'Data updated successfully',
								ResponseUpdateSuccess
							)
						);
					});
				} else {
					//User Already Exists. Then This Condtion Must Be Required.
					res.json(include.HTTP_RESPONSE.errorWithResponse('No Data found', []));
				}
			}
		);

		// include.kitchenFeature_schema.crud(req.body, 3, { _id: req.body.id }, (ResponseResultArray, ResponseGet) => {

		// });
	} else {
		// Validation Must Be Required.
		res.json(include.required.requiredParams(paramsInclude, req.body));
	}
});

router.post(include.constant.methods.getKitchenDetailbyId, function (req, res, next) {
	var paramsInclude = ['kitchen_id' /* ,'category_id' */ ];
	let base_url = req.protocol + '://' + req.get('host');
	if (include.required.requiredParams(paramsInclude, req.body) == true) {
		include.UserModel.getKitchenDetailbyId(req.body, (ErrorResponse, SuccessResponse, status) => {
			//console.log(ErrorResponse, SuccessResponse);
			if (SuccessResponse != null) {
				// @ Response Set Defiend.......
				res.json(include.HTTP_RESPONSE.successWithResponse('listing', SuccessResponse));
			} else {
				//User Already Exists. Then This Condtion Must Be Required.
				res.json(include.HTTP_RESPONSE.errorWithResponse('No Data found', ErrorResponse));
			}
		});
	} else {
		// Validation Must Be Required.
		res.json(include.required.requiredParams(paramsInclude, req.body));
	}
});

router.post(include.constant.methods.getAllPostalCodes, function (req, res, next) {
	let base_url = req.protocol + '://' + req.get('host');
	include.kitchenFeature_schema.crud(null, 6, null, (ResponseResultArray, ResponseGet) => {
		if (ResponseGet === 500) {
			res.json(include.HTTP_RESPONSE.errorWithResponse('No Data found', []));
		} else {
			res.json(include.HTTP_RESPONSE.successWithResponse('listing', ResponseResultArray));
		}
	});
});

router.post(include.constant.methods.imageUpload, function (req, res, next) {
	upload(req, res, function (err) {
		//console.log(req.files)
		if (
			req.files == undefined ||
			req.body.id == undefined ||
			req.body.id == null ||
			req.body.imagetype == null ||
			//req.body.schema == undefined ||
			req.body.imagetype == undefined
		) {
			//console.log("first if",req.file);

			res.json(
				include.HTTP_RESPONSE.errorWithResponse('Error please check your req params', [{
					error: true,
					message: 'Error please check your req params'
				}])
			);
			return;
		}

		var id = req.body.id /* ,schema = req.body.schema */ ,
			imagetype = req.body.imagetype,
			filename = '';

		var updateData = {};
		//console.log('body', req.body);
		//console.log('file', req.files);

		if (imagetype == 'images') {
			// updateData[imagetype] = filearray;
			include.KitchenSchema.crud(updateData, 1, {
				_id: id
			}, (ResponseData, ResponseST) => {
				if (ResponseData.length > 0) {
					var filearray = [];
					filearray = ResponseData[0].images;
					req.files.forEach((element) => {
						filearray.push(element.filename);
					});
					console.log(filearray);
					// ResponseData[0].images = filearray;
					// 		if (ResponseData[0].images.length<=5) {
					include.KitchenSchema.crud({
							images: filearray
						},
						3, {
							_id: id
						},
						(ResponseResultArray, ResponseGet) => {
							if (ResponseGet == 200) {
								res.json(
									include.HTTP_RESPONSE.successWithResponse('uploaded successfully', [{
										error: false,
										message: 'uploaded successfully'
									}])
								);
								return;
							} else {
								res.json(
									include.HTTP_RESPONSE.errorWithResponse('Something went wrong please try again', [{
										error: true,
										message: 'Error please check your req params'
									}])
								);
								return;
							}
						}
					);
				}
			});
		} else {
			updateData[imagetype] = req.files[0].filename;
			//console.log(updateData);
			include.UserSchema.crud(updateData, 3, {
				_id: id
			}, (ResponseResultArray, ResponseGet) => {
				if (ResponseGet == 200) {
					res.json(
						include.HTTP_RESPONSE.successWithResponse('uploaded successfully', [{
							error: false,
							message: 'uploaded successfully'
						}])
					);
					return;
				} else {
					res.json(
						include.HTTP_RESPONSE.errorWithResponse('Error please check your req params', [{
							error: true,
							message: 'Error please check your req params'
						}])
					);
					return;
				}
			});
		}
	});
});

router.get('/images/:filename', function (req, res, next) {
	var fs = require('fs');
	// For windows and linux support
	// var isWin = /^win/i.test(process.platform);

	var profilePath = './images/';
	var options = {
		root: profilePath,
		dotfiles: 'deny',
		headers: {
			'x-timestamp': Date.now(),
			'x-sent': true
		}
	};

	var fileName = req.params.filename;

	fs.exists(profilePath + '' + fileName, function (exists) {
		if (!exists) {
			fileName = 'default_profile_pic.jpg';
		}
		//console.log('path',fileName);

		res.sendFile(fileName, options, function (err) {
			if (err) {
				next(err);
			} else {
				//console.log('Sent:', fileName);
			}
		});
	});
});

router.post(include.constant.methods.signUpUsers, function (req, res, next) {
	var paramsInclude = [
		'address',
		'full_name',
		'contact',
		'postal_code',
		'email',
		'profile_pic',
		'user_type',
		'country_code',
		'password'
	];
	let base_url = req.protocol + '://' + req.get('host');
	//@ IF ........requiredParams
	if (include.required.requiredParams(paramsInclude, req.body) == true) {
		if (req.body.email == '') {
			res.json(include.HTTP_RESPONSE.errorWithResponse('Email can not be empty', []));
			return;
		}
		if (req.body.password == '') {
			res.json(include.HTTP_RESPONSE.errorWithResponse('Password can not be empty', []));
			return;
		}
		req.body.user = true;
		include.UserModel.openAnAccount(req.body, base_url, (ErrorResponse, SuccessResponse, status) => {
			if (ErrorResponse) {
				// @ Error Response code Here......
				res.json(include.HTTP_RESPONSE.errorsWithMessage(ErrorResponse));
			} else if (status == 200) {
				// @ Response Set Defiend.......
				res.json(
					include.HTTP_RESPONSE.successWithResponse(include.message.message.signup_success, SuccessResponse)
				);
			} else if (status == 204) {
				//User Already Exists. Then This Condtion Must Be Required.
				res.json(include.HTTP_RESPONSE.errorWithResponse(include.message.message.signup_account_exists, []));
			}
		});
		//@ End Else Here........
	} else {
		// Validation Must Be Required.
		res.json(include.required.requiredParams(paramsInclude, req.body));
	}
});

router.post(include.constant.methods.getEndUsers, function (req, res, next) {
	include.EndUserSchema.crud(null, 0, null, (ResponseResultArray, ResponseGet) => {
		if (ResponseGet === 500) {
			res.json(include.HTTP_RESPONSE.errorWithResponse('No Data found', []));
		} else {
			res.json(include.HTTP_RESPONSE.successWithResponse('Loading list', ResponseResultArray));
		}
	});
});

router.post(include.constant.methods.forgotPassword, function (req, res, next) {
	var paramsInclude = [
		'email',
		'usertype'
	]
	if (include.required.requiredParams(paramsInclude, req.body) == true) {
		include.UserModel.forgotPassword(req.body, (ErrorResponse, SuccessResponse, status) => {
			if (status == 500) {
				res.json(include.HTTP_RESPONSE.errorWithResponse('No Data found', []));
			} else {
				res.json(include.HTTP_RESPONSE.successWithResponse('Loading', SuccessResponse));
			}
		});
	} else {
		// Validation Must Be Required.
		res.json(include.required.requiredParams(paramsInclude, req.body));
	}
});

router.post(include.constant.methods.resetPassword, function (req, res, next) {
	var paramsInclude = [
		'email', 'new_password',
		'usertype'
	]
	if (include.required.requiredParams(paramsInclude, req.body) == true) {
		include.UserModel.resetPassword(req.body, (ErrorResponse, SuccessResponse, status) => {
			if (status == 500) {
				res.json(include.HTTP_RESPONSE.errorWithResponse('No Data found', ErrorResponse));
			} else {
				res.json(include.HTTP_RESPONSE.successWithResponse('Loading', SuccessResponse));
			}
		});
	} else {
		// Validation Must Be Required.
		res.json(include.required.requiredParams(paramsInclude, req.body));
	}
});


router.post(include.constant.methods.countryImport, function (req, res, next) {
	include.stripe_model.countryimport((ResponseResultArray, ResponseGet) => {
		if (ResponseGet === 500) {
			res.json(include.HTTP_RESPONSE.errorWithResponse('No Data found', []));
		} else {
			res.json(include.HTTP_RESPONSE.successWithResponse('Loading list', ResponseResultArray));
		}
	});
});

router.post(include.constant.methods.getcountrylist, function (req, res, next) {
	// include.countrySchema.crud(null, 0, null, (ResponseResultArray, ResponseGet) => {
	// 	if (ResponseGet === 500) {
	// 		res.json(include.HTTP_RESPONSE.errorWithResponse('No Data found', []));
	// 	} else {
	// 		res.json(include.HTTP_RESPONSE.successWithResponse('Loading list', ResponseResultArray));
	// 	}
	// });
	include.stripe_model.subscribeUserStripe(req.body, (data) => {
		res.json(include.HTTP_RESPONSE.errorWithResponse('Response', data));
	});
});

router.post(include.constant.methods.addBankAccountTostripe, function (req, res, next) {
	var paramsInclude = ['kitchen_id', 'country_code', 'currency', 'username', 'acc_no', 'routing_number', 'dob'];
	let base_url = req.protocol + '://' + req.get('host');
	//@ IF ........requiredParams
	if (include.required.requiredParams(paramsInclude, req.body) == true) {
		include.stripe_model.addBankAccStripe(req.body, (ResponseResultArray, ResponseGet) => {
			console.log(ResponseResultArray, ResponseGet);
			if (ResponseGet != null) {
				res.json(include.HTTP_RESPONSE.errorWithResponse("Unable to add account", ResponseGet));
			} else {
				res.json(include.HTTP_RESPONSE.successWithResponse('Success', ResponseResultArray));
			}
		});
	} else {
		// Validation Must Be Required.
		res.json(include.required.requiredParams(paramsInclude, req.body));
	}
});

router.post(include.constant.methods.transferapi, function (req, res, next) {
	include.stripe_model.paymentSplitStripe({}, (Response, error) => {
		if (error) {
			res.json(include.HTTP_RESPONSE.errorWithResponse('Something went wrong', error));
		} else {
			res.json(include.HTTP_RESPONSE.successWithResponse('Loading list', Response));
		}
	});
});

router.post(include.constant.methods.deleteItemImage, function (req, res, next) {
	var params = ['item_id', 'imagename'];

	if (include.required.required(params, req.body) == true) {
		include.KitchenModel.deleteItemImage(req.body, (err, success, status) => {
			if (status == 200) {
				res.json(include.HTTP_RESPONSE.successWithResponse(success[0].message, success));
			} else {
				res.json(include.HTTP_RESPONSE.errorWithResponse(err[0].message, err));
			}
		})
	} else {
		res.json(include.required.required(params, req.body));
	}

});

router.post(include.constant.methods.setDefaultImage, function (req, res, next) {
	var params = ['item_id', 'imagename'];

	if (include.required.required(params, req.body) == true) {
		include.KitchenModel.setDefaultImage(req.body, (err, success, status) => {
			if (status == 200) {
				res.json(include.HTTP_RESPONSE.successWithResponse(success[0].message, success));
			} else {
				res.json(include.HTTP_RESPONSE.errorWithResponse(err[0].message, err));
			}
		})
	} else {
		res.json(include.required.required(params, req.body));
	}

});

router.post(include.constant.methods.getsubUrbs, function (req, res, next) {
	include.KitchenModel.getsubUrbs(req.body.searchParam, (err, success, status) => {
		if (status == 200) {
			res.json(include.HTTP_RESPONSE.successWithResponse(success[0].message, success));
		} else {
			res.json(include.HTTP_RESPONSE.errorWithResponse(err[0].message, err));
		}
	})

});

var storageID = multer.diskStorage({
	//multers disk storage settings
	destination: function (req, file, cb) {
		cb(null, './images/docs/');
	},
	filename: function (req, file, cb) {
		var datetimestamp = Date.now();
		cb(
			null,
			file.fieldname +
			'-' +
			datetimestamp +
			'.' +
			file.originalname.split('.')[file.originalname.split('.').length - 1]
		);
	}
});

var uploadID = multer({
	//multer settings
	storage: storageID
	// }).any( /* 'image', 5 */ );
}).single('document', 1);

router.post(include.constant.methods.uploadLegalDoc, function (req, res, next) {
	console.log(typeof uploadID);
	uploadID(req, res, function (err) {
		console.log(err);
		console.log("req.file", req.file)
		console.log("req.files", req.files)
		console.log("req.headers", req.headers)
		console.log('update param', req.body);

		if (req.file == undefined || req.body.kitchen_id == undefined || req.body.kitchen_id == null) {
			res.json(
				include.HTTP_RESPONSE.errorWithResponse('Error please check your req params', [{
					error: true,
					message: 'Error please check your req params'
				}])
			);
			return;
		}
		include.stripe_model.uploadLegalDoc(req.body, req.file.filename, (error, success, status) => {
			if (error) {
				res.json(include.HTTP_RESPONSE.errorWithResponse(error[0].message, error))
			} else {
				res.json(include.HTTP_RESPONSE.successWithResponse(success[0].message, success))
			}
		})
	});
});
router.post(include.constant.methods.getListedAcc, function (req, res, next) {
	var params = ['kitchen_id'];

	if (include.required.required(params, req.body) == true) {

		include.stripe_model.getListedAcc(req.body, (error, success, status) => {
			if (error) {
				res.json(include.HTTP_RESPONSE.errorWithResponse(error[0].message, error))
			} else {
				res.json(include.HTTP_RESPONSE.successWithResponse("Listing success", success))
			}
		})
	} else {
		res.json(include.required.required(params, req.body));
	}
});
module.exports = router;