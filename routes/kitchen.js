var express = require('express');
var router = express.Router();
let include = require('../lib/include');

router.post(include.constant.methods.addMenuByKitchen, function (req, res, next) {
	//console.log('inisde');
	var paramsInclude = ['item_name', 'category_id', 'price', 'images', 'description', 'kitchen_id', 'quantity'];
	let base_url = req.protocol + '://' + req.get('host');
	//@ IF ........requiredParams
	if (include.required.requiredParams(paramsInclude, req.body) == true) {
		include.KitchenModel.addMenuItems(req.body, (ErrorResponse, SuccessResponse, status) => {
			if (ErrorResponse) {
				// @ Error Response code Here......
				res.json(include.HTTP_RESPONSE.errorWithResponse(ErrorResponse.message, []));
			} else if (status == 200) {
				// @ Response Set Defiend.......
				res.json(include.HTTP_RESPONSE.successWithResponse('Item added Successfully', SuccessResponse));
			} else if (status == 204) {
				//User Already Exists. Then This Condtion Must Be Required.
				res.json(include.HTTP_RESPONSE.successWithResponse('Item Already Exists', []));
			}
		});
		//@ End Else Here........
	} else {
		// Validation Must Be Required.
		res.json(include.required.requiredParams(paramsInclude, req.body));
	}
});

router.post(include.constant.methods.updateMenuItembyId, function (req, res, next) {
	//console.log('inisde');
	var paramsInclude = [
		'item_id' /* ,'item_name', 'category_id', 'price', 'images', 'description', 'kitchen_id','quantity' */
	];
	let base_url = req.protocol + '://' + req.get('host');
	//@ IF ........requiredParams
	if (include.required.requiredParams(paramsInclude, req.body) == true) {
		include.KitchenModel.updateMenuItems(req.body, (ErrorResponse, SuccessResponse, status) => {
			if (ErrorResponse) {
				// @ Error Response code Here......
				res.json(include.HTTP_RESPONSE.errorWithResponse('Item Already Exists', []));
			} else if (status == 200) {
				// @ Response Set Defiend.......
				res.json(include.HTTP_RESPONSE.successWithResponse('Item added Successfully', SuccessResponse));
			} else if (status == 204) {
				//User Already Exists. Then This Condtion Must Be Required.
				res.json(include.HTTP_RESPONSE.successWithResponse('Item Already Exists', []));
			}
		});
		//@ End Else Here........
	} else {
		// Validation Must Be Required.
		res.json(include.required.requiredParams(paramsInclude, req.body));
	}
});

router.post(include.constant.methods.listCategories, function (req, res, next) {
	include.MasterCategorySchema.crud(null, 1, {}, (SuccessResponse, ResponseGet) => {
		res.json(include.HTTP_RESPONSE.successWithResponse('Success', SuccessResponse));
	});
});

router.post(include.constant.methods.listCategories, function (req, res, next) {
	include.MasterCategorySchema.crud(null, 1, {}, (SuccessResponse, ResponseGet) => {
		res.json(include.HTTP_RESPONSE.successWithResponse('Success', SuccessResponse));
	});
});

router.post(include.constant.methods.addCategories, function (req, res, next) {
	include.MasterCategorySchema.crud(req.body, 2, null, (SuccessResponse, ResponseGet) => {
		res.json(include.HTTP_RESPONSE.successWithResponse('Success', SuccessResponse));
	});
});

router.post(include.constant.methods.excelImportById, function (req, res, next) {
	include.KitchenSchema.crud(null, 5, {
		kitchen_id: req.body.kitchen_id
	}, (ResponseResultArray, ResponseGet) => {
		res.json(include.HTTP_RESPONSE.successWithResponse('Success', ResponseResultArray));
	});
});

router.post(include.constant.methods.listMenuByKitchen, function (req, res, next) {
	var paramsInclude = ['kitchen_id'];
	let base_url = req.protocol + '://' + req.get('host');
	//@ IF ........requiredParams
	if (include.required.requiredParams(paramsInclude, req.body) == true) {
		include.KitchenModel.listMenuByKitchen(req.body, base_url, (ErrorResponse, SuccessResponse, status) => {
			if (ErrorResponse) {
				// @ Error Response code Here......
				res.json(include.HTTP_RESPONSE.errorsWithMessage('No Item found for this Kitchen'));
			} else if (status == 200) {
				// @ Response Set Defiend.......
				res.json(include.HTTP_RESPONSE.successWithResponse('Success', SuccessResponse));
			} else if (status == 204) {
				//User Already Exists. Then This Condtion Must Be Required.
				res.json(include.HTTP_RESPONSE.successWithResponse('Something went wrong', []));
			}
		});
		//@ End Else Here........
	} else {
		// Validation Must Be Required.
		res.json(include.required.requiredParams(paramsInclude, req.body));
	}
});

/*router.post(include.constant.methods.excelImportById, function(req, res, next) {
    var paramsInclude = ['kitchen_id'];
    let base_url = req.protocol + '://' + req.get('host');
    //@ IF ........requiredParams
    if (include.required.requiredParams(paramsInclude, req.body) == true) {

        include.KitchenModel.addMenuItems(req.body, base_url, (ErrorResponse, SuccessResponse, status) => {
            if (ErrorResponse) {

                // @ Error Response code Here......
                res.json(include.HTTP_RESPONSE.errorsWithMessage(ErrorResponse));
            } else if (status == 200) {

                // @ Response Set Defiend.......
                res.json(include.HTTP_RESPONSE.successWithResponse("Item added Successfully", SuccessResponse));
            }
            //User Already Exists. Then This Condtion Must Be Required.
            else if (status == 204) {
                res.json(include.HTTP_RESPONSE.successWithResponse("Item Already Exists", []));
            }
        });
        //@ End Else Here........
    } else {
        // Validation Must Be Required.
        res.json(include.required.requiredParams(paramsInclude, req.body));
    }

});

*/
router.post(
	include.constant.methods.excelImportById,
	function (req, res, next) {
		var paramsInclude = ['kitchen_id'];
		let base_url = req.protocol + '://' + req.get('host');
		//@ IF ........requiredParams
		if (include.required.requiredParams(paramsInclude, req.body) == true) {
			include.KitchenModel.addMenuItems(req.body, base_url, (ErrorResponse, SuccessResponse, status) => {
				if (ErrorResponse) {
					// @ Error Response code Here......
					res.json(include.HTTP_RESPONSE.errorsWithMessage(ErrorResponse));
				} else if (status == 200) {
					// @ Response Set Defiend.......
					res.json(include.HTTP_RESPONSE.successWithResponse('Item added Successfully', SuccessResponse));
				} else if (status == 204) {
					//User Already Exists. Then This Condtion Must Be Required.
					res.json(include.HTTP_RESPONSE.successWithResponse('Item Already Exists', []));
				}
			});
			//@ End Else Here........
		} else {
			// Validation Must Be Required.
			res.json(include.required.requiredParams(paramsInclude, req.body));
		}
	}
);

var multer = require('multer');

var storage = multer.diskStorage({
	//multers disk storage settings
	destination: function (req, file, cb) {
		cb(null, './profile_pic/');
	},
	filename: function (req, file, cb) {
		var datetimestamp = Date.now();
		cb(
			null,
			file.fieldname +
			'-' +
			datetimestamp +
			'.' +
			file.originalname.split('.')[file.originalname.split('.').length - 1]
		);
	}
});

var upload = multer({
	//multer settings
	storage: storage
}).single('profile_pic', 1);
//=================================================================================================================.

router.post(include.constant.methods.updateMenuByKitchen, function (req, res, next) {
	//console.log('called');
	// upload(req, res, function(err) {
	//console.log('update param', req.body);

	var responseInsert = {};
	var status_update = false;
	if (req.body.item_name != '' && req.body.item_name != null) {
		responseInsert.item_name = req.body.item_name;
		status_update = true;
	}
	if (req.body.category_id != '' && req.body.category_id != null) {
		responseInsert.category_id = req.body.category_id;
		status_update = true;
	}
	if (req.body.price != '' && req.body.price != null) {
		responseInsert.price = req.body.price;
		status_update = true;
	}
	if (req.body.description != '' && req.body.description != null) {
		responseInsert.description = req.body.description;
		status_update = true;
	}
	if (req.body.calories != '' && req.body.calories != null) {
		responseInsert.calories = req.body.calories;
		status_update = true;
	}
	if (req.body.fat != '' && req.body.fat != null) {
		responseInsert.fat = req.body.fat;
		status_update = true;
	}
	if (req.body.fat_unit != '' && req.body.fat_unit != null) {
		responseInsert.fat_unit = req.body.fat_unit;
		status_update = true;
	}
	if (req.body.carbohydrate != '' && req.body.carbohydrate != null) {
		responseInsert.carbohydrate = req.body.carbohydrate;
		status_update = true;
	}
	if (req.body.carbohydrate_unit != '' && req.body.carbohydrate_unit != null) {
		responseInsert.carbohydrate_unit = req.body.carbohydrate_unit;
		status_update = true;
	}
	if (req.body.protein != '' && req.body.protein != null) {
		responseInsert.protein = req.body.protein;
		status_update = true;
	}
	if (req.body.Protein_unit != '' && req.body.Protein_unit != null) {
		responseInsert.Protein_unit = req.body.Protein_unit;
		status_update = true;
	}
	if (req.body.quantity != undefined && req.body.quantity != null) {
		responseInsert.quantity = req.body.quantity;
		status_update = true;
	}
	if (status_update == true) {
		include.KitchenSchema.crud(responseInsert, 3, {
			_id: req.body.menu_id
		}, (ResponseResultArray, ResponseGet) => {
			if (ResponseGet == 200) {
				res.json(include.HTTP_RESPONSE.successWithResponse('Your account update successfully.', []));
			} else {
				res.json(include.HTTP_RESPONSE.errorWithResponse('Something went wrong.', []));
			}
		});
	}
});

router.post(include.constant.methods.searchFood, function (req, res, next) {
	var params = ['postal_code', 'requirement', 'price'];

	if (include.required.required(params, req.body) == true) {
		include.KitchenModel.searchFood(req.body, (ErrorResposne, SuccessResponse, status) => {
			if (ErrorResposne) {
				// @ Error Response code Here......
				// @ Error Response code Here......
				res.json(include.HTTP_RESPONSE.errorsWithMessage(ErrorResposne));
			} else if (status == 200) {
				// @ Response Message Success Fully Here Coded By Definded
				// @ Response Set Defiend.......
				res.json(include.HTTP_RESPONSE.successWithResponse('Loading search results.', SuccessResponse));
			} else if (status == 500) {
				// @ Response Message Success Fully Here Coded By Definded
				// @ Response Set Defiend.......
				res.json(include.HTTP_RESPONSE.errorsWithMessage('Something went wrong.', SuccessResponse));
			}
		});
	} else {
		res.json(include.required.required(params, req.body));
	}
});

router.post(include.constant.methods.deleteKitchenMaster, function (req, res, next) {
	var params = ['kitchen_id', 'enabled'];

	if (include.required.required(params, req.body) == true) {
		include.UserSchema.crud({
				enabled: req.body.enabled
			},
			3, {
				_id: req.body.kitchen_id
			},
			(ResponseResultArray, ResponseGet) => {
				if (ResponseGet == 200) {
					var msg = req.body.enabled ? 'Kitchen Enabled successfully...' : 'Kitchen disabled successfully...';
					res.json(include.HTTP_RESPONSE.successWithResponse(msg, ResponseResultArray));
				} else {
					res.json(include.HTTP_RESPONSE.errorWithResponse('Something went wrong.', []));
				}
			}
		);
	} else {
		res.json(include.required.required(params, req.body));
	}
});

router.post(include.constant.methods.deleteFoodItems, function (req, res, next) {
	var params = ['item_id'];

	if (include.required.required(params, req.body) == true) {
		include.KitchenModel.deleteFoodItembyId(req.body, (err, success, status) => {
			if (status == 200) {
				res.json(include.HTTP_RESPONSE.successWithResponse(success[0].message, success));
			} else {
				res.json(include.HTTP_RESPONSE.errorWithResponse(err[0].message, err));
			}
		})
	} else {
		res.json(include.required.required(params, req.body));
	}

});


module.exports = router;